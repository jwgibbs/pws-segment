MODULE PSCS_4D_io_read_inputs

USE PSCS_constants
USE PSCS_4D_parallel_proc_split
USE PSCS_4D_io_misc

!=== Variables that will be read from the file =================================
IMPLICIT NONE
! File locations
  CHARACTER(char_len)  ::  input_dir = "/scratch/26B/tiny"
  CHARACTER(char_len)  ::  output_dir = "/scratch/26B/tiny"
  CHARACTER(char_len)  ::  file_basename = "26B"
! Dataset names
  CHARACTER(char_len)  ::  rho_dset = 'rho'
  CHARACTER(char_len)  ::  phi1_dset = 'phi_1'
  CHARACTER(char_len)  ::  phi2_dset = 'phi_2'
! File dimensions
  INTEGER  ::  x_min = -1
  INTEGER  ::  x_max = -1
  INTEGER  ::  y_min = -1
  INTEGER  ::  y_max = -1
  INTEGER  ::  z_min = -1
  INTEGER  ::  z_max = -1
  INTEGER  ::  t_min = 20
  INTEGER  ::  t_max = 30
! Segmentation variables
  INTEGER  ::  iterations = 20
  REAL(kind=MyR)  ::  phi_ds_time = 0.0
  REAL(kind=MyR)  ::  phi_mmc_time = 0.0
  REAL(kind=MyR)  ::  rec_smoothing = 0.0
  REAL(kind=MyR)  ::  dif_smoothing = 0.0
  REAL(kind=MyR)  ::  update_dt = 0.5
! Diffusivities
  REAL(kind=MyR)  ::  Dx = 1.0
  REAL(kind=MyR)  ::  Dy = 1.0
  REAL(kind=MyR)  ::  Dz = 1.0
  REAL(kind=MyR)  ::  Dt = 1.0
! Thresholding variables
  REAL(kind=MyR)  ::  phi1_threshold = 0.0
  REAL(kind=MyR)  ::  phi2_threshold = 1.0
! Dimensions
  INTEGER, DIMENSION(3)  ::  dims_file
  INTEGER, DIMENSION(4)  ::  dims_full
  INTEGER, DIMENSION(4)  ::  mpi_lim_min, mpi_lim_max
!===============================================================================

CONTAINS

SUBROUTINE ReadInputs ()
IMPLICIT NONE
!=== Variables for reading inputs ==============================================
  CHARACTER(len=100)  ::  inputs_fn
  CHARACTER(len=200)  ::  buffer, label, value
  INTEGER, PARAMETER  ::  file_id = 15
  INTEGER             ::  read_status, pos, line
! Formats for printing the results of reading the input file
  CHARACTER(len=100)  ::  int_fmt = "(2X,' | ',I6,' | ',A15,' | ',I8)"
  CHARACTER(len=100)  ::  flt_fmt = "(2X,' | ',I6,' | ',A15,' | ',F8.2)"
  CHARACTER(len=100)  ::  log_fmt = "(2X,' | ',I6,' | ',A15,' | ',L8)"
  CHARACTER(len=100)  ::  str_fmt = "(2X,' | ',I6,' | ',A15,' | ',A48)"
  CHARACTER(len=100)  ::  cmt_fmt = "(2X,' | ',I6,' | ',A15,' | ',A48)"
  
  CHARACTER(char_len)  ::  filename, fn_fmt = "(A, '/', A, '_', I4.4, '.h5')"

!=== Get the filename for the input file and open it ===========================
  CALL get_command_argument(1, inputs_fn)
  IF (LEN_TRIM(inputs_fn) == 0) inputs_fn='example_inputs.txt'
  
  !WRITE (*,*) TRIM(inputs_fn)
  open(file_id, file=inputs_fn)
!===========================================================
! read_status is negative if an end of record condition is 
! encountered or if an endfile condition was detected.  It 
! is positive if an error was detected.  read_status is
! zero otherwise.
!=== Read the input file =======================================
  read_status = 0
  line = 0
  IF (rank .EQ. 0) THEN
      WRITE (*, "(2X,' | ',A6,' | ',A15,' | ',A8)") " LINE ", " VARIABLE NAME ", "  VALUE  "
      WRITE (*, "(2X,' | ',A6,' | ',A15,' | ',A8)") "======", "===============", "========="
  END IF
    DO WHILE (read_status == 0)
        READ(file_id, '(A)', iostat=read_status) buffer
        IF (read_status == 0) THEN
            line = line + 1
            ! Remove the leading whitespace and make lowercase
            buffer = ADJUSTL(buffer)
            ! PRINT *, trim(buffer)
            IF (buffer(1:1) == "#") THEN
                   !IF (rank .EQ. 0) WRITE (*, cmt_fmt) line, "Comment", ""
            ELSE
                ! Find the first instance of whitespace.  Split label and data.
                pos = scan(buffer, '=:')
                ! 
                label = buffer(1:pos-1)
                label = ADJUSTL(label)
                CALL lowercase(label)
                ! 
                value = buffer(pos+1:)
                value = ADJUSTL(value)
                !print *, 'x', ADJUSTL(label), 'x', value, 'x'
                SELECT CASE (label)
                !=== File locations ============================================
                CASE ('input_dir')
                    READ(value, *, iostat=read_status) input_dir
                    IF (rank .EQ. 0) WRITE (*, str_fmt) line, "input_dir", input_dir
                CASE ('output_dir')
                    READ(value, *, iostat=read_status) output_dir
                    IF (rank .EQ. 0) WRITE (*, str_fmt) line, "output_dir", output_dir
                CASE ('file_basename')
                    READ(value, *, iostat=read_status) file_basename
                    IF (rank .EQ. 0) WRITE (*, str_fmt) line, "file_basename", file_basename
                !=== Dataset names =============================================
                CASE ('rho_dset')
                    READ(value, *, iostat=read_status) rho_dset
                    IF (rank .EQ. 0) WRITE (*, str_fmt) line, "rho_dset", rho_dset
                CASE ('phi1_dset')
                    READ(value, *, iostat=read_status) phi1_dset
                    IF (rank .EQ. 0) WRITE (*, str_fmt) line, "phi1_dset", phi1_dset
                CASE ('phi2_dset')
                    READ(value, *, iostat=read_status) phi2_dset
                    IF (rank .EQ. 0) WRITE (*, str_fmt) line, "phi2_dset", phi2_dset
                !=== File dimensions ===========================================
                CASE ('x_min')
                    READ(value, *, iostat=read_status) x_min
                    IF (rank .EQ. 0) WRITE (*, int_fmt) line, "x_min", x_min
                CASE ('x_max')
                    READ(value, *, iostat=read_status) x_max
                    IF (rank .EQ. 0) WRITE (*, int_fmt) line, "x_max", x_max
                CASE ('y_min')
                    READ(value, *, iostat=read_status) y_min
                    IF (rank .EQ. 0) WRITE (*, int_fmt) line, "y_min", y_min
                CASE ('y_max')
                    READ(value, *, iostat=read_status) y_max
                    IF (rank .EQ. 0) WRITE (*, int_fmt) line, "y_max", y_max
                CASE ('z_min')
                    READ(value, *, iostat=read_status) z_min
                    IF (rank .EQ. 0) WRITE (*, int_fmt) line, "z_min", z_min
                CASE ('z_max')
                    READ(value, *, iostat=read_status) z_max
                    IF (rank .EQ. 0) WRITE (*, int_fmt) line, "z_max", z_max
                CASE ('t_min')
                    READ(value, *, iostat=read_status) t_min
                    IF (rank .EQ. 0) WRITE (*, int_fmt) line, "t_min", t_min
                CASE ('t_max')
                    READ(value, *, iostat=read_status) t_max
                    IF (rank .EQ. 0) WRITE (*, int_fmt) line, "t_max", t_max
                !=== Segmentation variables ====================================
                CASE ('iterations')
                    READ(value, *, iostat=read_status) iterations
                    IF (rank .EQ. 0) WRITE (*, int_fmt) line, "iterations", iterations
                CASE ('phi_ds_time')
                    READ(value, *, iostat=read_status) phi_ds_time
                    IF (rank .EQ. 0) WRITE (*, flt_fmt) line, "phi_ds_time", phi_ds_time
                CASE ('phi_mmc_time')
                    READ(value, *, iostat=read_status) phi_mmc_time
                    IF (rank .EQ. 0) WRITE (*, flt_fmt) line, "phi_mmc_time", phi_mmc_time
                CASE ('rec_smoothing')
                    READ(value, *, iostat=read_status) rec_smoothing
                    IF (rank .EQ. 0) WRITE (*, flt_fmt) line, "rec_smoothing", rec_smoothing
                CASE ('dif_smoothing')
                    READ(value, *, iostat=read_status) dif_smoothing
                    IF (rank .EQ. 0) WRITE (*, flt_fmt) line, "dif_smoothing", dif_smoothing
                CASE ('update_dt')
                    READ(value, *, iostat=read_status) update_dt
                    IF (rank .EQ. 0) WRITE (*, flt_fmt) line, "update_dt", update_dt
                !=== Diffusivity variables =====================================
                CASE ('dx')
                    READ(value, *, iostat=read_status) Dx
                    IF (rank .EQ. 0) WRITE (*, flt_fmt) line, "Dx", Dx
                CASE ('dy')
                    READ(value, *, iostat=read_status) Dy
                    IF (rank .EQ. 0) WRITE (*, flt_fmt) line, "Dy", Dy
                CASE ('dz')
                    READ(value, *, iostat=read_status) Dz
                    IF (rank .EQ. 0) WRITE (*, flt_fmt) line, "Dz", Dz
                CASE ('dt')
                    READ(value, *, iostat=read_status) Dt
                    IF (rank .EQ. 0) WRITE (*, flt_fmt) line, "Dt", Dt
                !=== Thresholding variables ====================================
                CASE ('phi1_threshold')
                    READ(value, *, iostat=read_status) phi1_threshold
                    IF (rank .EQ. 0) WRITE (*, flt_fmt) line, "phi1_threshold", phi1_threshold
                CASE ('phi2_threshold')
                    READ(value, *, iostat=read_status) phi2_threshold
                    IF (rank .EQ. 0) WRITE (*, flt_fmt) line, "phi2_threshold", phi2_threshold
                !=== Fallback===================================================
                CASE DEFAULT
                   !IF (rank .EQ. 0) WRITE (*,*) 'Skipping invalid label at line', line
                !===================================================
                END SELECT
            END IF
        
        END IF
    END DO
    
    
    WRITE (filename, fn_fmt) TRIM(input_dir), TRIM(file_basename), t_min
    IF (rank == 0) PRINT *, 'Get dimensions from ', TRIM(filename)
    dims_file = get_dims(filename, rho_dset)

    ! Add some sanity checks to this...
    IF (x_min == -1) x_min = 1
    IF (x_max == -1) x_max = dims_file(1)
    IF (y_min == -1) y_min = 1
    IF (y_max == -1) y_max = dims_file(2)
    IF (z_min == -1) z_min = 1
    IF (z_max == -1) z_max = dims_file(3)


    IF (rank == 0) WRITE (*,*) 'x dimensions: ', x_min, x_max
    IF (rank == 0) WRITE (*,*) 'y dimensions: ', y_min, y_max
    IF (rank == 0) WRITE (*,*) 'z dimensions: ', z_min, z_max

    dims_full(1) = x_max - x_min + 1
    dims_full(2) = y_max - y_min + 1
    dims_full(3) = z_max - z_min + 1
    dims_full(4) = t_max - t_min + 1

    IF (rank == 0) PRINT *, 'split procs'
    CALL split_procs (np, dims_full)
    CALL GenIndices (rank, dims_full, mpi_lim_min, mpi_lim_max)

    IF (x_min /= -1) THEN
        mpi_lim_min(1) = mpi_lim_min(1) + x_min
        mpi_lim_max(1) = mpi_lim_max(1) + x_min
    END IF

    IF (y_min /= -1) THEN
        mpi_lim_min(2) = mpi_lim_min(2) + y_min
        mpi_lim_max(2) = mpi_lim_max(2) + y_min
    END IF
    
    IF (z_min /= -1) THEN
        mpi_lim_min(3) = mpi_lim_min(3) + z_min
        mpi_lim_max(3) = mpi_lim_max(3) + z_min
    END IF
    
    mpi_lim_min(4) = t_min
    mpi_lim_max(4) = t_max
    !===========================================================
    
    
END SUBROUTINE ReadInputs
!===============================================================
SUBROUTINE LOWERCASE(STR)
  !=============================================================
  IMPLICIT NONE
  CHARACTER(LEN=*), INTENT(IN OUT) :: STR
  INTEGER :: I, DEL
  !=============================================================
  DEL = IACHAR('a') - IACHAR('A')
  
  DO I = 1, LEN_TRIM(STR)
     IF (LGE(STR(I:I),'A') .AND. LLE(STR(I:I),'Z')) THEN
        STR(I:I) = ACHAR(IACHAR(STR(I:I)) + DEL)
     END IF
  END DO
  
  RETURN

END SUBROUTINE LOWERCASE
!===============================================================

END MODULE PSCS_4D_io_read_inputs
