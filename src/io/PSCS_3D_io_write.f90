MODULE PSCS_3D_io_write
! Functions for reading and writing HDF files
! ==============================================================
USE HDF5
USE PSCS_constants
USE PSCS_3D_parallel_proc_split
IMPLICIT NONE
CONTAINS
! ==============================================================
SUBROUTINE write_3D_to_3D (filename, dsetname, src_arr, dims_full)
!=============================================================
! + Calculate indices and offsets
! + Check to see if the file exists
! + Create a file access property list for opening the file
! + Open the file
! |  + Close the file access property list
! |  + Check to see if the dataset exists 
! |  + Open the dataset
! |  |  + Get dataspace identifier for the dataset
! |  |  + Create a dataspace for the memory
! |  |  + Select a hyperslab in the file-dataspace
! |  |  + Select a hyperslab in the memory-dataspace
! |  |  + Create property list for collective dataset write
! |  |  + Read the dataset
! |  |  + Close the dataset write property list, dataspaces, ...
! |  + Close the dataset
! + Close the file
!=============================================================
IMPLICIT NONE
! Inputs:
  CHARACTER(LEN=char_len), INTENT(IN)  ::  filename         ! File name
  CHARACTER(LEN=char_len), INTENT(IN)  ::  dsetname         ! Dataset name
  
  INTEGER, DIMENSION(3), INTENT(IN)  ::  dims_full
  
  REAL(kind=MyR), DIMENSION(0:, 0:, 0:), INTENT(IN)  ::  src_arr
!=== HDF variables =============================================
  INTEGER(HID_T)  ::  file_id       ! File identifier
  INTEGER(HID_T)  ::  plist_id      ! Property list for parallel reads
  INTEGER(HID_T)  ::  dset_id       ! Dataset identifier
  INTEGER(HID_T)  ::  dcpl_id       ! Data creation property list identifier
  INTEGER(HID_T)  ::  dspace_id     ! Dataspace identifier
  INTEGER(HID_T)  ::  mem_id        ! Memory space identifier
  INTEGER         ::  hdf_err       ! Error flag
!=============================================================
! Local variables:
  INTEGER, DIMENSION(3)  ::  lim_min, lim_max

  INTEGER, PARAMETER  ::  ndim_src  = 3
  INTEGER, PARAMETER  ::  ndim_file = 3
  
  INTEGER(HSIZE_T)  ::  nx_write, nx_src, x_min, x_max
  INTEGER(HSIZE_T)  ::  ny_write, ny_src, y_min, y_max
  INTEGER(HSIZE_T)  ::  nz_write, nz_src, z_min, z_max
  INTEGER(HSIZE_T)  ::  nt_write, nt_src, t_min, t_max

  INTEGER(HSIZE_T), DIMENSION(ndim_src)  :: dims_src_full
  INTEGER(HSIZE_T), DIMENSION(ndim_src)  :: dims_src_write
  INTEGER(HSIZE_T), DIMENSION(ndim_file) :: dims_file_full
  INTEGER(HSIZE_T), DIMENSION(ndim_file) :: dims_file_write
  INTEGER(HSIZE_T), DIMENSION(ndim_file) :: dims_chunk

  INTEGER(HSIZE_T), DIMENSION(ndim_src)  :: offset_src
  INTEGER(HSIZE_T), DIMENSION(ndim_file) :: offset_file

  LOGICAL  ::  link_exists, file_exists
! === Get all necessary dimensions =============================
  dims_src_full = SHAPE(src_arr)
  nx_src = dims_src_full(1) - 2 * gc
  ny_src = dims_src_full(2) - 2 * gc
  nz_src = dims_src_full(3) - 2 * gc

  CALL GenIndices (rank, dims_full, lim_min, lim_max)

  x_min = lim_min(1)
  x_max = lim_max(1)
  y_min = lim_min(2)
  y_max = lim_max(2)
  z_min = lim_min(3)
  z_max = lim_max(3)

  nx_write = x_max - x_min + 1
  ny_write = y_max - y_min + 1
  nz_write = z_max - z_min + 1

  dims_src_write  = (/nx_write, ny_write, nz_write/)
  dims_file_write = (/nx_write, ny_write, nz_write/)

  dims_file_full = (/dims_full(1), dims_full(2), dims_full(3)/)

  dims_chunk(1) = min(32, dims_full(1))
  dims_chunk(2) = min(32, dims_full(2))
  dims_chunk(3) = min(32, dims_full(3))

  offset_src  = (/0, 0, 0/)
  offset_file = (/x_min-1, y_min-1, z_min-1/)
! === Open or create the file ==================================================
  ! Setup file access property list to open the file 
    CALL h5pcreate_f(H5P_FILE_ACCESS_F, plist_id, hdf_err)
    CALL h5pset_fapl_mpio_f(plist_id, MPI_COMM_WORLD, MPI_INFO_NULL, hdf_err)

    INQUIRE(file=filename, exist=file_exists)

    IF (file_exists .eqv. .FALSE.) THEN
        ! Print a status message
          !IF (rank == 0) WRITE(*,*) 
          IF (rank == 0) WRITE(*,*) 'Creating a new file: ', TRIM(filename)
        ! Create the new file
          CALL h5fcreate_f(filename, H5F_ACC_TRUNC_F, file_id, hdf_err, access_prp = plist_id)
    ELSE
        ! Print a status message
          !IF (rank == 0) WRITE(*,*) 
          IF (rank == 0) WRITE(*,*) 'Using an existing file: ', TRIM(filename)
        ! Open the existing file
          CALL h5fopen_f (filename, H5F_ACC_RDWR_F, file_id, hdf_err, access_prp=plist_id)
    END IF

    ! Close the file access property list
      CALL h5pclose_f(plist_id, hdf_err)

! === Create the dataset =======================================================
  ! Check to see if the dataspace exists
    CALL h5lexists_f(file_id, dsetname, link_exists, hdf_err)

  ! Delete any existing datasets
    IF (link_exists .eqv. .TRUE.) THEN
        IF (rank == 0) WRITE(*,*) '    Dataset ', TRIM(dsetname), ' is being deleted and overwritten'
        CALL h5ldelete_f(file_id, dsetname, hdf_err)
    ELSE
        IF (rank == 0) WRITE(*,*) '    Dataset ', TRIM(dsetname), ' is being created'
    END IF
  
  ! Create the dataspace
    CALL h5screate_simple_f(ndim_file, dims_file_full, dspace_id, hdf_err)

  ! Create the dataset creation property list
    CALL h5pcreate_f(H5P_DATASET_CREATE_F, dcpl_id, hdf_err)

  ! Set the chunk size
    CALL h5pset_chunk_f(dcpl_id, ndim_file, dims_chunk, hdf_err)

  ! Create the dataset with default properties.
    CALL h5dcreate_f(file_id, dsetname, H5T_NATIVE_REAL, dspace_id, dset_id, hdf_err, dcpl_id)

  ! Close the DCPL
    CALL h5pclose_f(dcpl_id, hdf_err)

! === Write the dataset ========================================================
  ! Select hyperslab in the dataset.
    CALL h5sselect_hyperslab_f(dspace_id, H5S_SELECT_SET_F, offset_file, dims_file_write, hdf_err)

  ! Create memory dataspace.
    CALL h5screate_simple_f(ndim_src, dims_src_write, mem_id, hdf_err)

  ! Select hyperslab in memory.
    CALL h5sselect_hyperslab_f(mem_id, H5S_SELECT_SET_F, offset_src, dims_src_write, hdf_err)

  ! Create property list for collective dataset write
    CALL h5pcreate_f(H5P_DATASET_XFER_F, plist_id, hdf_err) 
    CALL h5pset_dxpl_mpio_f(plist_id, H5FD_MPIO_COLLECTIVE_F, hdf_err)
  
  ! Write data from hyperslab in memory into the hyperslab in the file
    CALL H5dwrite_f(dset_id, H5T_NATIVE_REAL, src_arr(1:nx_src, 1:ny_src, 1:nz_src), &
                    dims_src_write, hdf_err, mem_space_id=mem_id, file_space_id=dspace_id, &
                    xfer_prp=plist_id)

  ! Close the H5P_DATASET_XFER property list
    CALL h5pclose_f(plist_id, hdf_err)

  ! Close the dataset
    CALL h5dclose_f(dset_id, hdf_err)

  ! Close the dataspace
    CALL h5sclose_f(dspace_id, hdf_err)

  ! Close the memoryspace.
    CALL h5sclose_f(mem_id, hdf_err)

  ! Close the file.
    CALL h5fclose_f(file_id, hdf_err)

END SUBROUTINE write_3D_to_3D


END MODULE PSCS_3D_io_write
