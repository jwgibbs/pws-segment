MODULE PSCS_4D_io_read
! Functions for reading and writing HDF files
!===============================================================================
USE HDF5
USE PSCS_constants
IMPLICIT NONE
CONTAINS
!===============================================================================
SUBROUTINE read_3D_to_4D(filename, dsetname, dest_arr, lim_min, lim_max, time)
IMPLICIT NONE
!===============================================================================
! + Calculate indices and offsets
! + Check to see if the file exists
! + Open the file
! |  + Check to see if the dataset exists
! |  + Open the dataset
! |  |  + Get dataspace identifier for the dataset
! |  |  + Create a dataspace for the memory
! |  |  + Select a hyperslab in the file-dataspace
! |  |  + Select a hyperslab in the memory-dataspace
! |  |  + Read the dataset
! |  |  + Close the memory-dataspace, file-dataspace
! |  + Close the dataset
! + Close the file
!=== Inputs ====================================================================
  CHARACTER(LEN=char_len), INTENT(IN)  ::  filename         ! File name
  CHARACTER(LEN=char_len), INTENT(IN)  ::  dsetname         ! Dataset name
  
  INTEGER, DIMENSION(4), INTENT(IN)  ::  lim_min, lim_max
  INTEGER, INTENT(IN)  ::  time

  REAL(kind=MyR), DIMENSION(0:, 0:, 0:, 0:)  ::  dest_arr
!=== HDF variables =============================================================
  INTEGER(HID_T)  ::  file_id       ! File identifier
  INTEGER(HID_T)  ::  plist_id      ! Property list for parallel reads
  INTEGER(HID_T)  ::  dset_id       ! Dataset identifier
  INTEGER(HID_T)  ::  dspace_id     ! Dataspace identifier
  INTEGER(HID_T)  ::  mem_id        ! Memory space identifier
  INTEGER         ::  hdf_err       ! Error flag
!=== Local variables ===========================================================
  INTEGER, PARAMETER  ::  ndim_dest = 4
  INTEGER, PARAMETER  ::  ndim_file = 3
  
  INTEGER(HSIZE_T)  ::  nx_read, nx_dest, x_min, x_max
  INTEGER(HSIZE_T)  ::  ny_read, ny_dest, y_min, y_max
  INTEGER(HSIZE_T)  ::  nz_read, nz_dest, z_min, z_max
  INTEGER(HSIZE_T)  ::  nt_read, nt_dest, t_min, t_max

  INTEGER(HSIZE_T), DIMENSION(ndim_dest) :: dims_dest_full
  INTEGER(HSIZE_T), DIMENSION(ndim_dest) :: dims_dest_read
  INTEGER(HSIZE_T), DIMENSION(ndim_file) :: dims_file_full
  INTEGER(HSIZE_T), DIMENSION(ndim_file) :: dims_file_read

  INTEGER(HSIZE_T), DIMENSION(ndim_dest) :: offset_dest
  INTEGER(HSIZE_T), DIMENSION(ndim_file) :: offset_file
  ! ==============================================================
  ! Get all necessary dimensions:
    dims_dest_full = SHAPE(dest_arr)
    nx_dest = dims_dest_full(1) - 2 * gc
    ny_dest = dims_dest_full(2) - 2 * gc
    nz_dest = dims_dest_full(3) - 2 * gc
    nt_dest = dims_dest_full(4) - 2 * gc
    
    x_min = lim_min(1)
    x_max = lim_max(1)
    y_min = lim_min(2)
    y_max = lim_max(2)
    z_min = lim_min(3)
    z_max = lim_max(3)

    nx_read = x_max - x_min + 1
    ny_read = y_max - y_min + 1
    nz_read = z_max - z_min + 1
    nt_read = 1

    dims_dest_read = (/nx_read, ny_read, nz_read, nt_read/)
    dims_file_read = (/nx_read, ny_read, nz_read/)

    offset_dest = (/0, 0, 0, 0/)
    offset_file = (/x_min-1, y_min-1, z_min-1/)



  ! Check the dimensions of the file 
  ! Make sure the input limits are smaller than the dataset dimensions



  ! Setup file access property list with parallel I/O access.
    CALL h5pcreate_f(H5P_FILE_ACCESS_F, plist_id, hdf_err)
    CALL h5pset_fapl_mpio_f(plist_id, MPI_COMM_WORLD, MPI_INFO_NULL, hdf_err)
  
  ! Open the file.
    CALL h5fopen_f (filename, H5F_ACC_RDONLY_F, file_id, hdf_err, access_prp=plist_id)
    !CALL h5fopen_f (filename, H5F_ACC_RDWR_F, file_id, hdf_err)

  ! Close the H5P_FILE_ACCESS property list:
    CALL h5pclose_f(plist_id, hdf_err)
  
  ! Open the dataset.
    CALL h5dopen_f(file_id, dsetname, dset_id, hdf_err)

  ! Get dataset's dataspace identifier.
    CALL h5dget_space_f(dset_id, dspace_id, hdf_err)

  ! Select hyperslab in the dataset.
    CALL h5sselect_hyperslab_f(dspace_id, H5S_SELECT_SET_F, offset_file, dims_file_read, hdf_err)

  ! Create memory dataspace.
    CALL h5screate_simple_f(ndim_dest, dims_dest_read, mem_id, hdf_err)

  ! Select hyperslab in memory.
    CALL h5sselect_hyperslab_f(mem_id, H5S_SELECT_SET_F, offset_dest, dims_dest_read, hdf_err)

  ! Read data from hyperslab in the file into the hyperslab in memory.
    CALL H5dread_f(dset_id, H5T_NATIVE_REAL, dest_arr(1:nx_dest, 1:ny_dest, 1:nz_dest, time), &
                   dims_file_read, hdf_err, mem_id, dspace_id)

  ! Close the dataspace for the dataset.
    CALL h5sclose_f(dspace_id, hdf_err)

  ! Close the memoryspace.
    CALL h5sclose_f(mem_id, hdf_err)

  ! Close the dataset.
    CALL h5dclose_f(dset_id, hdf_err)

  ! Close the file.
    CALL h5fclose_f(file_id, hdf_err)

  ! Apply BCs to fill in empty edge cells
    !CALL const_slope_BC(dest_arr)

END SUBROUTINE read_3D_to_4D



END MODULE PSCS_4D_io_read
