MODULE PSCS_4D_io_misc
! Functions for reading and writing HDF files
! ==============================================================
USE HDF5
USE PSCS_constants
IMPLICIT NONE
CONTAINS
! ==============================================================
FUNCTION get_dims(filename, dsetname)
  !=============================================================
  ! + Open the file
  ! |  + Open the dataset
  ! |  |  + Get the dataspace identifier
  ! |  |  + Open the dataspace
  ! |  |  |  + Get the size of the dataset
  ! |  |  + Close the dataspace
  ! |  + Close the dataset
  ! + Close the file
  !=============================================================
  IMPLICIT NONE
  
  CHARACTER(LEN=char_len), INTENT(IN)  ::  filename         ! File name
  CHARACTER(LEN=char_len), INTENT(IN)  ::  dsetname         ! Dataset name
  
  INTEGER(HSIZE_T), DIMENSION(3) :: dset_dims, max_dims
  INTEGER, DIMENSION(3) :: get_dims
  
  INTEGER(HID_T)  ::  file_id       ! File identifier
  INTEGER(HID_T)  ::  dset_id       ! Dataset identifier
  INTEGER(HID_T)  ::  dspace_id     ! Dataspace identifier
  INTEGER(HID_T)  ::  plist_id      ! Property list for parallel reads
  INTEGER         ::  hdf_err       ! Error flag
  !=============================================================
  ! Setup file access property list with parallel I/O access.
    CALL h5pcreate_f(H5P_FILE_ACCESS_F, plist_id, hdf_err)
    CALL h5pset_fapl_mpio_f(plist_id, MPI_COMM_WORLD, MPI_INFO_NULL, hdf_err)
  
  ! Open an existing file as read only
  CALL h5fopen_f (filename, H5F_ACC_RDONLY_F, file_id, hdf_err, access_prp = plist_id)
  
  ! Open an existing dataset.
  CALL h5dopen_f(file_id, dsetname, dset_id, hdf_err)
  
  ! Close the H5P_FILE_ACCESS property list:
  CALL h5pclose_f(plist_id, hdf_err)
  
  ! Get dataset's dataspace identifier and select subset.
  CALL h5dget_space_f(dset_id, dspace_id, hdf_err)
  
  ! + Read the dimensions of the array
  CALL h5sget_simple_extent_dims_f(dspace_id, dset_dims, max_dims, hdf_err)
  
  ! + Close the dataspace
  CALL h5sclose_f(dspace_id, hdf_err)
  
  ! + Close the dataset
  CALL h5dclose_f(dset_id, hdf_err)
  
  ! + Close the file
  CALL h5fclose_f(file_id, hdf_err)
  
  get_dims(1) = INT(dset_dims(1))
  get_dims(2) = INT(dset_dims(2))
  get_dims(3) = INT(dset_dims(3))

END function get_dims


END MODULE PSCS_4D_io_misc
