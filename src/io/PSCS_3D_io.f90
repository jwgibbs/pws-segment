MODULE PSCS_3D_io

! This module is just to give me easier access to the individual I/O modules
   USE PSCS_3D_io_read
   USE PSCS_3D_io_write
   USE PSCS_3D_io_misc
   USE PSCS_3D_io_read_inputs

END MODULE PSCS_3D_io
