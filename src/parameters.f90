MODULE parameters
!==============================================================
!  THIS MODULE IS INTENDED TO PROVIDE AN EASY METHOD OF SHARING
!  COMMONLY USED DATA.
!==============================================================
USE HDF5
USE PSCS_constants
IMPLICIT NONE
!==============================================================
! Inputs
  CHARACTER(char_len)  ::  filename = '../data/solidif_0005.h5'
  CHARACTER(char_len)  ::  dsetname = 'rho'
! Outputs
  CHARACTER(char_len)  ::  out_file = 'test_out.h5'
  CHARACTER(char_len)  ::  rho_dset = 'rho'
  CHARACTER(char_len)  ::  phi_dset = 'phi'
  CHARACTER(char_len)  ::  liq_dset = 'liquid'
  CHARACTER(char_len)  ::  sol_dset = 'solid'
  CHARACTER(char_len)  ::  out_dset = 'sample'
!==============================================================
END MODULE parameters
