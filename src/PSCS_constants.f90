MODULE PSCS_constants
!==============================================================
! A module for sharing constants and datatypes across the
! program
!==============================================================
!USE MPI
IMPLICIT NONE
INCLUDE 'mpif.h'
!=== Datatypes =================================================
  ! Single (SGL), double (DBL) and 'my real' (MyR)
  INTEGER, PARAMETER  ::  SGL = SELECTED_REAL_KIND (p=06)
  INTEGER, PARAMETER  ::  DBL = SELECTED_REAL_KIND (p=13)
  INTEGER, PARAMETER  ::  MyR = SGL
  ! Default character length:
  INTEGER, PARAMETER  ::  char_len = 128
!=== Some MPI parameters =======================================
  INTEGER  ::  np
  INTEGER  ::  rank
  INTEGER, DIMENSION(4)  :: np_i
!=== Miscellaneous stuff =======================================
  ! Number of ghost cells on the array bounds:
  INTEGER, PARAMETER  ::  gc = 1
  REAL(kind=MyR), PARAMETER   :: pi = 3.14159265358979323_MyR
!==============================================================
END MODULE PSCS_constants
