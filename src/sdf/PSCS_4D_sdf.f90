MODULE PSCS_4D_sdf
! =============================================================
USE PSCS_4D_parallel
USE PSCS_constants
IMPLICIT NONE
! =============================================================
CONTAINS

FUNCTION reinit (arr, max_it)
! ==============================================================
!
!  Reinitializes a signed distance function using first order
!  finite differencing to find the gradient of arr and
!  Godunov's method for finding the upwind values.  More info
!  about initializing or reinitializing a signed distance function
!  can be found in in "Level Set Methods and Dynamic Implicit
!  Surfaces" by Osher and Fedkiw.
!
! ==============================================================
  IMPLICIT NONE

  ! Inputs:
  INTEGER, INTENT(IN)  ::  max_it
  REAL(kind=MyR), DIMENSION(0:, 0:, 0:, 0:), INTENT(IN)  :: arr

  ! Outputs:
  REAL(kind=MyR), DIMENSION(:,:,:,:), ALLOCATABLE  :: reinit

  ! In/out:

  ! Local:
  REAL(kind=MyR)  ::  dt  = 0.40
  REAL(kind=MyR)  ::  tol = 0.25
  REAL(kind=MyR)  ::  band = 50.0
  REAL(kind=MyR), DIMENSION(:,:,:,:), ALLOCATABLE   :: arr_t
  REAL(kind=MyR)  :: sgn, gXm, gXp, gYm, gYp, gZm, gZp, grad
  REAL(kind=MyR)  :: zero=0.0
  REAL(kind=MyR)  :: max_val
  REAL(kind=MyR)  :: max_val_t
  INTEGER         :: mpi_err
  INTEGER         :: iter, num
  INTEGER         :: x, y, z, t
  INTEGER         :: nx, ny, nz, nt
  INTEGER, DIMENSION(4) :: dims
  CHARACTER(64)   :: fmt1, fmt2
  ! ============================================================
  !WRITE (*,'( 2X, A )') "REINITIALIZING THE SIGNED DISTANCE FUNCTION"
  fmt1 = "(A5, 2X, A10, 2X, A10,   2X, A10)"
  fmt2 = "(I5, 2X, I10, 2X, F10.3, 2X, F10.1)"
 ! WRITE (*,fmt1) "     ", " Num still", "     Max  ", "          "
 ! WRITE (*,fmt1) " Iter", "    moving", "   |phi_t|", " Max |phi|"
 ! WRITE (*,fmt1) "=====", "==========", "==========", "=========="
  
  dims = shape(arr)
  nx = dims(1) - 2 * gc
  ny = dims(2) - 2 * gc
  nz = dims(3) - 2 * gc
  nt = dims(4) - 2 * gc
  
  ALLOCATE (  arr_t ( 0:nx+1, 0:ny+1, 0:nz+1, 0:nt+1 ) )
  ALLOCATE ( reinit ( 0:nx+1, 0:ny+1, 0:nz+1, 0:nt+1 ) )
  arr_t = 0.0
  reinit = arr
  
  iter = 0
  ! ============================================================
  DO iter = 1, max_it
    !iter = iter + 1
    num = 0
    DO t = 1, nt
      DO z = 1,nz
        DO Y = 1,ny
          DO x = 1,nx
            !======================================================
            ! ONLY UPDATE THE reinit VALUES WHERE reinit IS WITHIN THE
            ! NARROW-BAND.
            IF (ABS(reinit(x,y,z,t)) .GE. band) THEN
              arr_t(x,y,z,t) = 0.0
            ELSE
              !======================================================
              ! CALCULATE THE 'SMEARED' SIGN OF reinit.  THE SMEARING
              ! IS DONE WITH THE +10.0 IN THE SQRT AND IS DONE TO
              ! SLOW THE RATE AT WHICH VALUES NEAR THE INTERFACE 
              ! CHANGE.
              sgn = reinit(x,y,z,t)/SQRT( reinit(x,y,z,t)**2 + 10.0 )
              !======================================================
              ! CALCULATE BOTH BACKWARD (e.g. gXm) AND FORWARD 
              ! (e.g. gXp) DERIVATIVES
              gXm = reinit(X,  Y,  Z  , t) - reinit(X-1,Y,  Z  , t)
              gXp = reinit(X+1,Y,  Z  , t) - reinit(X,  Y,  Z  , t)
              gYm = reinit(X,  Y,  Z  , t) - reinit(X,  Y-1,Z  , t)
              gYp = reinit(X,  Y+1,Z  , t) - reinit(X,  Y,  Z  , t)
              gZm = reinit(X,  Y,  Z  , t) - reinit(X,  Y,  Z-1, t)
              gZp = reinit(X,  Y,  Z+1, t) - reinit(X,  Y,  Z  , t)
              !======================================================
              ! USE ONLY UPWIND VALUES USING GODUNOV'S METHOD:
              IF (sgn .GT. 0.0) THEN
                gXm = MAX(+gXm, zero)
                gXp = MAX(-gXp, zero)
                gYm = MAX(+gYm, zero)
                gYp = MAX(-gYp, zero)
                gZm = MAX(+gZm, zero)
                gZp = MAX(-gZp, zero)
              ELSE
                gXm = MAX(-gXm, zero)
                gXp = MAX(+gXp, zero)
                gYm = MAX(-gYm, zero)
                gYp = MAX(+gYp, zero)
                gZm = MAX(-gZm, zero)
                gZp = MAX(+gZp, zero)
              END IF
              grad = MAX(gXm, gXp)**2 + MAX(gYm, gYp)**2 + MAX(gZm, gZp)**2
              !======================================================
              ! WHILE THE DEVIATION (dev) BETWEEN THE GRADIENT AND 
              ! THE TARGET VALUE OF 1.0 IS ABOVE THE TOLERANCE (tol), 
              ! KEEP UPDATING reinit.  IF (dev < tol) DO NOT UPDATE
              ! BECAUSE IT WILL CAUSE EXCESSIVE MOTION OF THE 
              ! INTERFACE.
              arr_t(x,y,z,t) = sgn * ( 1.0 - SQRT(grad) )
              IF ( 1.0 - SQRT(grad) .GT. tol ) THEN
                  num = num + 1
              END IF
              !======================================================
            END IF
          END DO
        END DO
      END DO
    END DO
    !======================================================
    reinit = reinit + dt * arr_t
  
    !WHERE (reinit > +band) reinit = +band
    !WHERE (reinit < -band) reinit = -band
    !======================================================
    ! APPLY BOUNDARY CONDITIONS
    CALL const_slope_BC(reinit)
    !======================================================
    !max_val   = MAXVAL(ABS(reinit))
    !max_val_t = MAXVAL(ABS(arr_t))
    !WRITE (*,fmt2) iter, num, max_val_t, max_val
    !======================================================
    !IF (num .EQ. 0) EXIT
    !IF (iter .EQ. max_it) EXIT
    !======================================================
  END DO

 ! WRITE (*,'( 4X, A, I5 )') "Reinitialization iterations: ", iter
  !======================================================
  DEALLOCATE( arr_t )
  CALL MPI_BARRIER (MPI_COMM_WORLD, mpi_err )
  !======================================================
END FUNCTION reinit


END MODULE PSCS_4D_sdf
