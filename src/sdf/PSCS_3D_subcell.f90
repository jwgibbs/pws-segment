MODULE PSCS_3D_subcell
! =============================================================
USE PSCS_3D_parallel
USE PSCS_constants
IMPLICIT NONE
! =============================================================
CONTAINS

FUNCTION subcell (arr, max_it)
! ==============================================================
!
!  Reinitializes a signed distance function using first order
!  finite differencing to find the gradient of arr and
!  Godunov's method for finding the upwind values.  More info
!  about initializing or reinitializing a signed distance function
!  can be found in in "Level Set Methods and Dynamic Implicit
!  Surfaces" by Osher and Fedkiw.
!
! ==============================================================
  IMPLICIT NONE

  ! Inputs:
  INTEGER, INTENT(IN)  ::  max_it
  REAL(kind=MyR), DIMENSION(0:, 0:, 0:), INTENT(IN)  :: arr

  ! Outputs:
  REAL(kind=MyR), DIMENSION(:,:,:), ALLOCATABLE  :: subcell

  ! In/out:

  ! Local:
  REAL(kind=MyR)  ::  dt  = 0.40
  REAL(kind=MyR)  ::  tol = 0.25
  REAL(kind=MyR)  ::  band = 50.0
  REAL(kind=MyR), DIMENSION(:,:,:), ALLOCATABLE   :: arr_t
  REAL(kind=MyR)  :: gX, gXm, gXp, gY, gYm, gYp, gZ, gZm, gZp
  REAL(kind=MyR)  :: sgn, dist, grad
  REAL(kind=MyR)  :: V1, V2, V3, V4, V5
  REAL(kind=MyR)  :: zero=0.0
  REAL(kind=MyR)  :: max_val
  REAL(kind=MyR)  :: max_val_t
  INTEGER         ::  mpi_err
  INTEGER         :: iter, num
  INTEGER         :: x, y, z
  INTEGER         :: nx, ny, nz
  INTEGER, DIMENSION(3) :: dims
  CHARACTER(64)   :: fmt1, fmt2
  LOGICAL         :: interface_voxel
  ! ============================================================
  !WRITE (*,'( 2X, A )') "REINITIALIZING THE SIGNED DISTANCE FUNCTION"
  fmt1 = "(A5, 2X, A10, 2X, A10,   2X, A10)"
  fmt2 = "(I5, 2X, I10, 2X, F10.3, 2X, F10.1)"
 ! WRITE (*,fmt1) "     ", " Num still", "     Max  ", "          "
 ! WRITE (*,fmt1) " Iter", "    moving", "   |phi_t|", " Max |phi|"
 ! WRITE (*,fmt1) "=====", "==========", "==========", "=========="
  
  dims = shape(arr)
  nx = dims(1) - 2 * gc
  ny = dims(2) - 2 * gc
  nz = dims(3) - 2 * gc
  
  ALLOCATE (  arr_t ( 0:nx+1, 0:ny+1, 0:nz+1 ) )
  ALLOCATE ( subcell ( 0:nx+1, 0:ny+1, 0:nz+1 ) )
  arr_t = 0.0
  subcell = arr
  
  iter = 0
  ! ============================================================
  DO iter = 1, max_it
    !iter = iter + 1
    num = 0
    DO z = 1,nz
      DO Y = 1,ny
        DO x = 1,nx
          arr_t(x, y, z) = 0.0
          sgn = SIGN(arr(x, y, z), 1.0)
          !--- Determine if a voxel is on the interface ------------------------
          interface_voxel = .False.
          IF (arr(x, y, z) * arr(x-1, y, z) < 0.0) interface_voxel = .True.
          IF (arr(x, y, z) * arr(x+1, y, z) < 0.0) interface_voxel = .True.
          IF (arr(x, y, z) * arr(x, y-1, z) < 0.0) interface_voxel = .True.
          IF (arr(x, y, z) * arr(x, y+1, z) < 0.0) interface_voxel = .True.
          IF (arr(x, y, z) * arr(x, y, z-1) < 0.0) interface_voxel = .True.
          IF (arr(x, y, z) * arr(x, y, z+1) < 0.0) interface_voxel = .True.
          !--- Do the interface boundary voxels first ------------
          IF (interface_voxel .EQV. .True.) THEN
            gX = 0.0
            gX = MAX(gX, ABS((arr(x+1, y, z) - arr(x-1, y, z)) / 2.0))
            gX = MAX(gX, ABS((arr(x+1, y, z) - arr(x-0, y, z)) / 1.0))
            gX = MAX(gX, ABS((arr(x+0, y, z) - arr(x-1, y, z)) / 1.0))
            gX = MAX(gX, 1e-9)
            
            gY = 0.0
            gY = MAX(gY, ABS((arr(x, y+1, z) - arr(x, y-1, z)) / 2.0))
            gY = MAX(gY, ABS((arr(x, y+1, z) - arr(x, y-0, z)) / 1.0))
            gY = MAX(gY, ABS((arr(x, y+0, z) - arr(x, y-1, z)) / 1.0))
            gY = MAX(gY, 1e-9)
            
            gZ = 0.0
            gZ = MAX(gZ, ABS((arr(x, y, z+1) - arr(x, y, z-1)) / 2.0))
            gZ = MAX(gZ, ABS((arr(x, y, z+1) - arr(x, y, z-0)) / 1.0))
            gZ = MAX(gZ, ABS((arr(x, y, z+0) - arr(x, y, z-1)) / 1.0))
            gZ = MAX(gZ, 1e-9)
            
            dist = arr(x, y, z) / sqrt(gX**2 + gY**2 + gZ**2)
            arr_t(x, y, z) = dist - sgn * ABS(subcell(x, y, z))
          !--- Do the non-boundary voxels next ---------------------------------
          ELSE
            !--- backward derivative in x-direction ---
            V1 = subcell(x-2, y, z) - subcell(x-3, y, z)
            V2 = subcell(x-1, y, z) - subcell(x-2, y, z)
            V3 = subcell(x+0, y, z) - subcell(x-1, y, z)
            V4 = subcell(x+1, y, z) - subcell(x+0, y, z)
            V5 = subcell(x+2, y, z) - subcell(x+1, y, z)
            gXm = WENO(V1, V2, V3, V4, V5)
            !--- forward derivative in x-direction---
            V1 = subcell(x+3, y, z) - subcell(x+2, y, z)
            V2 = subcell(x+2, y, z) - subcell(x+1, y, z)
            V3 = subcell(x+1, y, z) - subcell(x+0, y, z)
            V4 = subcell(x+0, y, z) - subcell(x-1, y, z)
            V5 = subcell(x-1, y, z) - subcell(x-2, y, z)
            gXp = WENO(V1, V2, V3, V4, V5)
            !--- backward derivative in y-direction ---
            V1 = subcell(x, y-2, z) - subcell(x, y-3, z)
            V2 = subcell(x, y-1, z) - subcell(x, y-2, z)
            V3 = subcell(x, y+0, z) - subcell(x, y-1, z)
            V4 = subcell(x, y+1, z) - subcell(x, y+0, z)
            V5 = subcell(x, y+2, z) - subcell(x, y+1, z)
            gYm = WENO(V1, V2, V3, V4, V5)
            !--- forward derivative in y-direction ---
            V1 = subcell(x, y+3, z) - subcell(x, y+2, z)
            V2 = subcell(x, y+2, z) - subcell(x, y+1, z)
            V3 = subcell(x, y+1, z) - subcell(x, y+0, z)
            V4 = subcell(x, y+0, z) - subcell(x, y-1, z)
            V5 = subcell(x, y-1, z) - subcell(x, y-2, z)
            gYp = WENO(V1, V2, V3, V4, V5)
            !--- backward derivative in z-direction ---
            V1 = subcell(x, y, z-2) - subcell(x, y, z-3)
            V2 = subcell(x, y, z-1) - subcell(x, y, z-2)
            V3 = subcell(x, y, z+0) - subcell(x, y, z-1)
            V4 = subcell(x, y, z+1) - subcell(x, y, z+0)
            V5 = subcell(x, y, z+2) - subcell(x, y, z+1)
            gZm = WENO(V1, V2, V3, V4, V5)
            !--- forward derivative in z-direction ---
            V1 = subcell(x, y, z+3) - subcell(x, y, z+2)
            V2 = subcell(x, y, z+2) - subcell(x, y, z+1)
            V3 = subcell(x, y, z+1) - subcell(x, y, z+0)
            V4 = subcell(x, y, z+0) - subcell(x, y, z-1)
            V5 = subcell(x, y, z-1) - subcell(x, y, z-2)
            gZp = WENO(V1, V2, V3, V4, V5)
            !--- use only upwind values using godunov's method ---
            IF (sgn > zero) THEN
              gXm = MAX(gXm, zero)
              gXp = MIN(gXp, zero)
              gYm = MAX(gYm, zero)
              gYp = MIN(gYp, zero)
              gZm = MAX(gZm, zero)
              gZp = MIN(gZp, zero)
            ELSE
              gXm = MIN(gXm, zero)
              gXp = MAX(gXp, zero)
              gYm = MIN(gYm, zero)
              gYp = MAX(gYp, zero)
              gZm = MIN(gZm, zero)
              gZp = MAX(gZp, zero)
            END IF
            gX = MAX(gXm**2, gXp**2)
            gY = MAX(gYm**2, gYp**2)
            gZ = MAX(gZm**2, gZp**2)
            grad = SQRT(gX + gY + gZ)
            arr_t(x, y, z) = sgn * (1.0 - grad)
          END IF ! interface boundary / non-boundary if
          !======================================================
          IF ( arr_t(x,y,z) .GT. tol ) THEN
              num = num + 1
          END IF
        END DO ! x-loop
      END DO ! y-loop
    END DO ! z-loop
    !======================================================
    subcell = subcell + dt * arr_t
  
    !WHERE (subcell > +band) subcell = +band
    !WHERE (subcell < -band) subcell = -band
    !======================================================
    ! APPLY BOUNDARY CONDITIONS
    CALL const_slope_BC(subcell)
    !======================================================
    !max_val   = MAXVAL(ABS(subcell))
    !max_val_t = MAXVAL(ABS(arr_t))
    !WRITE (*,fmt2) iter, num, max_val_t, max_val
    !======================================================
    !IF (num .EQ. 0) EXIT
    !IF (iter .EQ. max_it) EXIT
    !======================================================
  END DO

 ! WRITE (*,'( 4X, A, I5 )') "Reinitialization iterations: ", iter
  !======================================================
  DEALLOCATE( arr_t )
  CALL MPI_BARRIER (MPI_COMM_WORLD, mpi_err )
  !======================================================
END FUNCTION subcell

FUNCTION WENO(V1, V2, V3, V4, V5)
  !  Based on the WENO method presented in Osher and Fedkiw's 
  !  "Level set methods and dynamic implic surfaces"
  !--- Inputs ------------------------------------------------------------------
  REAL(kind=MyR)  ::  V1, V2, V3, V4, V5
  !--- Outputs -----------------------------------------------------------------
  REAL(kind=MyR)  ::  WENO
  !--- Locals ------------------------------------------------------------------
  REAL(kind=MyR)  ::  S1, S2, S3
  REAL(kind=MyR)  ::  W1, W2, W3
  REAL(kind=MyR)  ::  A1, A2, A3
  REAL(kind=MyR)  ::  G1, G2, G3
  REAL(kind=MyR)  ::  eps = 1e-6
  !-----------------------------------------------------------------------------
  S1 = + (13./12.) * (1.*V1 - 2.*V2 + 1.*V3)**2 &
       + ( 1./ 4.) * (1.*V1 - 4.*V2 + 3.*V3)**2
  S2 = + (13./12.) * (1.*V2 - 2.*V3 + 1.*V4)**2 &
       + ( 1./ 4.) * (1.*V2 - 1.*V4)**2
  S3 = + (13./12.) * (1.*V3 - 2.*V4 + 1.*V5)**2 &
       + ( 1./ 4.) * (3.*V3 - 4.*V2 + 1.*V5)**2
  !-----------------------------------------------------------------------------
  A1 = 0.1/(S1+eps)**2
  A2 = 0.6/(S2+eps)**2
  A3 = 0.3/(S3+eps)**2
  !-----------------------------------------------------------------------------
  W1 = A1/(A1+A2+A3)
  W2 = A2/(A1+A2+A3)
  W3 = A3/(A1+A2+A3)
  !-----------------------------------------------------------------------------
  G1 = (1./6.) * (+ 2.*V1 - 7.*V2 + 11.*V3)
  G2 = (1./6.) * (- 1.*V2 + 5.*V3 +  2.*V4)
  G3 = (1./6.) * (+ 2.*V3 + 5.*V4 -  1.*V5)
  !-----------------------------------------------------------------------------
  WENO = W1*G1 + W2*G2 + W3*G3

END FUNCTION WENO

END MODULE PSCS_3D_subcell
