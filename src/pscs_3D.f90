MODULE pscs_3D

  ! Various constants:
    USE PSCS_constants

  ! input/output sub-modules:
    USE PSCS_3D_io

  ! parallel and boundary condition sub-modules:
    USE PSCS_3D_parallel

  ! 
    USE PSCS_3D_sdf

  ! 
    USE PSCS_3D_smooth

END MODULE pscs_3D
