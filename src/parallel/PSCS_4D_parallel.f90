MODULE PSCS_4D_parallel
  
  USE PSCS_4D_parallel_collective_ops
  USE PSCS_4D_parallel_BCs
  USE PSCS_4D_parallel_proc_split
  
END MODULE PSCS_4D_parallel
