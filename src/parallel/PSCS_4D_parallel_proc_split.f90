MODULE PSCS_4D_parallel_proc_split

USE PSCS_constants
IMPLICIT NONE

CONTAINS
!===============================================================
FUNCTION xyzt_to_rank (x, y, z, t)
! Implicit variables are evil
  IMPLICIT NONE
! Inputs
  INTEGER, INTENT(IN) :: x, y, z, t
! Outputs
  INTEGER             :: xyzt_to_rank
! Local
  ! None
! Do stuff
  xyzt_to_rank = + (x * 1) &
                 + (y * np_i(1)) &
                 + (z * np_i(1) * np_i(2)) &
                 + (t * np_i(1) * np_i(2) * np_i(3))
END FUNCTION xyzt_to_rank
!===============================================================
FUNCTION rank_to_xyzt (rank)
! Implicit variables are evil
  IMPLICIT NONE
! Inputs
  INTEGER, INTENT(IN)  ::  rank
! Outputs
  INTEGER, DIMENSION(4)  ::  rank_to_xyzt
! Local
  INTEGER  ::  id_x, id_y, id_z, id_t
! Get proc ID in each dimension
  id_X = MOD( (rank), np_i(1))
  id_Y = MOD( (rank - id_x) / np_i(1), np_i(2))
  id_Z = MOD( (rank - id_X - id_Y * np_i(1)) / (np_i(1) * np_i(2)), np_i(3))
  id_t = MOD( (rank - id_X - id_Y * np_i(1) - id_z * np_i(1) * np_i(2)) / (np_i(1) * np_i(2) * np_i(3)), np_i(4))
  
  rank_to_xyzt(1) = id_X
  rank_to_xyzt(2) = id_Y
  rank_to_xyzt(3) = id_Z
  rank_to_xyzt(4) = id_t
END FUNCTION rank_to_xyzt
!===============================================================


!===============================================================
SUBROUTINE split_procs (np, dims)
    IMPLICIT NONE
    
    INTEGER, INTENT(IN) ::  np
    INTEGER, DIMENSION(4), INTENT(IN)  ::  dims
    
    INTEGER  ::  rem, odd, num
    INTEGER  ::  nx, ny, nz, nt
    INTEGER  ::  np_X, np_y, np_Z, np_t
    !===========================================================
    nx = dims(1)
    ny = dims(2)
    nz = dims(3)
    nt = dims(4)
    
    np_x = 1
    np_y = 1
    np_z = 1
    np_t = 1
    
    rem = np - (np_X * np_Y * np_Z * np_t)
    odd = MOD(np / (np_X * np_Y * np_Z * np_t), 2)
    num = np / (np_x * np_y * np_z * np_t)
    ! ==========================================================
    ! Method 1:
    DO
        IF (num < 2) EXIT
        IF      ( nx/np_x >= max(ny/np_y, nz/np_z, nt/np_t) ) THEN
            np_x = np_x * 2
        ELSE IF ( ny/np_y >= max(nx/np_x, nz/np_z, nt/np_t) ) THEN
            np_y = np_y * 2
        ELSE IF ( nz/np_z >= max(nx/np_x, ny/np_y, nt/np_t) ) THEN
            np_z = np_z * 2
        ELSE IF ( nt/np_t >= max(nx/np_x, ny/np_y, nz/np_z)) THEN
            np_t = np_t * 2
        END IF
        num = np / (np_x * np_y * np_z * np_t)
    END DO
    np_z = np_z * num
    np_z = floor(REAL(np_z))
    ! ==========================================================
    ! Method 2:
    !DO
    !    IF      ((nx/np_x >= ny/np_y) .and. (nx/np_x >= nz/np_z)) THEN
    !        np_x = np_x * 2
    !    ELSE IF ((ny/np_y >= nx/np_x) .and. (ny/np_y >= nz/np_z)) THEN
    !        np_y = np_y * 2
    !    ELSE IF ((nz/np_z >= nx/np_x) .and. (nz/np_z >= ny/np_y)) THEN
    !        np_z = np_z * 2
    !    END IF
    !    num = np / (np_x * np_y * np_z)
    !    odd = MOD(np / (np_X * np_Y * np_Z), 2)
    !    IF (num < 2) EXIT
    !    IF (odd == 1) EXIT
    !END DO
    !np_z = np_z * num
    !np_z = floor(np_z)
    ! ==========================================================
    np_i(1) = np_x
    np_i(2) = np_y
    np_i(3) = np_z
    np_i(4) = np_t
    
    IF (rank == 1) WRITE (*,*) 'Processor layout: ', np_x, np_y, np_z, np_t
    IF (rank == 1) WRITE (*,*) 'Voxels per proc: ', nx/np_x, ny/np_y, nz/np_z, nt/np_t
END SUBROUTINE split_procs
!===============================================================


!===============================================================
SUBROUTINE GenIndices (rank, dims, mpi_lim_min, mpi_lim_max)
    !===========================================================
    !  GENERATE INDICES FOR A GIVEN PROCESSOR
    !===========================================================
    !
    !  id_X = rank  %  nX
    !
    !===========================================================
    !          (rank) - (id_X)
    !  id_Y = -----------------  %  nY
    !                 nX
    !===========================================================
    !          (rank) - (id_X) - (id_Y*np_Y)
    !  id_Z = -------------------------------  %  nZ
    !                      nX*nY
    !===========================================================
    IMPLICIT NONE

    INTEGER, INTENT(IN) :: rank
    INTEGER, DIMENSION(4), INTENT(IN)  ::  dims
    INTEGER, DIMENSION(4), INTENT(OUT)  ::  mpi_lim_min, mpi_lim_max
    
    INTEGER, DIMENSION(4) ::  my_xyzt

    INTEGER  ::  nx, ny, nz, nt
    INTEGER  ::  np_X, np_y, np_z, np_t
    INTEGER  ::  id_x, id_y, id_z, id_t
    INTEGER  ::  nx_MPI, ny_MPI, nz_MPI, nt_MPI
    INTEGER  ::  xmin_mpi, ymin_mpi, zmin_mpi, tmin_mpi
    INTEGER  ::  xmax_mpi, ymax_mpi, zmax_mpi, tmax_mpi
    !===========================================================
    nx = dims(1)
    ny = dims(2)
    nz = dims(3)
    nt = dims(4)

    np_x = np_i(1)
    np_y = np_i(2)
    np_z = np_i(3)
    np_t = np_i(4)

    my_xyzt = rank_to_xyzt(rank)
    id_X = my_xyzt(1)
    id_Y = my_xyzt(2)
    id_Z = my_xyzt(3)
    id_t = my_xyzt(4)

    Xmin_MPI = 1+(id_X+0)*nX/np_X
    Xmax_MPI = 0+(id_X+1)*nX/np_X
    nX_MPI = Xmax_MPI-Xmin_MPI+1

    Ymin_MPI = 1+(id_Y+0)*nY/np_Y
    Ymax_MPI = 0+(id_Y+1)*nY/np_Y
    nY_MPI = Ymax_MPI-Ymin_MPI+1

    Zmin_MPI = 1+(id_Z+0)*nz/np_Z
    Zmax_MPI = 0+(id_Z+1)*nz/np_Z
    nZ_MPI = Zmax_MPI-Zmin_MPI+1
    
    tmin_MPI = 1 + nt * (id_t + 0) / np_t
    tmax_MPI = 0 + nt * (id_t + 1) / np_t
    nt_MPI = tmax_MPI - tmin_MPI + 1
    
    mpi_lim_min(1) = Xmin_MPI
    mpi_lim_min(2) = Ymin_MPI
    mpi_lim_min(3) = Zmin_MPI
    mpi_lim_min(4) = tmin_MPI

    mpi_lim_max(1) = Xmax_MPI
    mpi_lim_max(2) = Ymax_MPI
    mpi_lim_max(3) = Zmax_MPI
    mpi_lim_max(4) = tmax_MPI

    !WRITE (*,'( 2X, I6, I6, I6, I6, I6 )') RANK, np_X, np_Y, np_Z, np_t
    !WRITE (*,'( 2X, I6, I6, I6, I6, I6 )') RANK, id_X, id_Y, id_Z, id_t
    !WRITE (*,'( 2X, I6, I6, I6, I6 )') RANK, Xmin_MPI, Xmax_MPI, nX_MPI
    !WRITE (*,'( 2X, I6, I6, I6, I6 )') RANK, Ymin_MPI, Ymax_MPI, nY_MPI
    !WRITE (*,'( 2X, I6, I6, I6, I6 )') RANK, Zmin_MPI, Zmax_MPI, nZ_MPI
    !WRITE (*,'( 2X, I6, I6, I6, I6 )') RANK, tmin_MPI, tmax_MPI, nt_MPI
    !WRITE (*,'( 2X, A, I3, 2X, A, I3, 2X, A, I3, 2X, A, I3 , A, I3 )') &
          !"RANK: ", RANK, "id_X=", id_X, "id_Y=", id_Y, "id_Z=", id_Z, "id_t=", id_t

END SUBROUTINE GenIndices
!===============================================================
END MODULE PSCS_4D_parallel_proc_split
