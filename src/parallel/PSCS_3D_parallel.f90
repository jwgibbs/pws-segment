MODULE PSCS_3D_parallel
  
  USE PSCS_3D_parallel_collective_ops
  USE PSCS_3D_parallel_BCs
  USE PSCS_3D_parallel_proc_split
  
END MODULE PSCS_3D_parallel
