MODULE PSCS_4D_parallel_collective_ops

USE PSCS_constants
IMPLICIT NONE

CONTAINS
!===============================================================
FUNCTION parallel_masked_mean (arr, mask)
! Implicit variables are evil
  IMPLICIT NONE
! Inputs
  REAL(MyR), DIMENSION(0:, 0:, 0:, 0:), INTENT(IN)  ::  arr
  LOGICAL,   DIMENSION(0:, 0:, 0:, 0:), INTENT(IN)  ::  mask
! Outputs
  REAL(MyR)  ::  parallel_masked_mean
! Local
  REAL(8)     ::  local_sum, total_sum
  REAL(8)     ::  local_num, total_num
  INTEGER(4)  ::  mpi_err
  INTEGER(4), DIMENSION(4)  ::  dims
  INTEGER(4)  ::  x, y, z, t
  INTEGER(4)  ::  nx, ny, nz, nt
  
  dims = shape(arr)
  nx = dims(1) - 2 * gc
  ny = dims(2) - 2 * gc
  nz = dims(3) - 2 * gc
  nt = dims(4) - 2 * gc
  
  local_sum = 0.0
  local_num = 0

    DO t = 1, nt
      DO z = 1, nz
        DO y = 1, ny
          DO x = 1, nx
          IF (mask(x,y,z,t) .EQV. .TRUE.) THEN
            local_sum = local_sum + arr(x,y,z,t)
            local_num = local_num + 1
          END IF
        END DO
      END DO
    END DO
  END DO

  !local_sum = SUM(arr, mask=mask)
  !local_num = COUNT(mask)
  
  CALL MPI_ALLREDUCE(local_sum, total_sum, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, mpi_err)
  CALL MPI_ALLREDUCE(local_num, total_num, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, mpi_err)
  
  !IF (rank == 0) WRITE (*,*) 'total_sum = ', total_sum
  !IF (rank == 0) WRITE (*,*) 'total_num = ', total_num

  parallel_masked_mean = REAL(total_sum / (REAL(total_num, kind=8) + 1E-6_dbl), kind=4)
END FUNCTION parallel_masked_mean
!===============================================================
FUNCTION parallel_masked_max (arr, mask)
! Implicit variables are evil
  IMPLICIT NONE
! Inputs
  REAL(MyR), DIMENSION(0:, 0:, 0:, 0:), INTENT(IN)  ::  arr
  LOGICAL,   DIMENSION(0:, 0:, 0:, 0:), INTENT(IN)  ::  mask
! Outputs
  REAL(4)  ::  parallel_masked_max
! Local
  REAL(4)  ::  arr_max
  INTEGER    ::  mpi_err

  arr_max = MAXVAL(arr, mask=mask)
  CALL MPI_ALLREDUCE(arr_max, parallel_masked_max, 1, MPI_REAL, MPI_MAX, MPI_COMM_WORLD, mpi_err)
END FUNCTION parallel_masked_max
!===============================================================
FUNCTION parallel_masked_min (arr, mask)
! Implicit variables are evil
  IMPLICIT NONE
! Inputs
  REAL(MyR), DIMENSION(0:, 0:, 0:, 0:), INTENT(IN)  ::  arr
  LOGICAL,   DIMENSION(0:, 0:, 0:, 0:), INTENT(IN)  ::  mask
! Outputs
  REAL(4)  ::  parallel_masked_min
! Local
  REAL(4)  ::  arr_min
  INTEGER    ::  mpi_err

  arr_min = MINVAL(arr, mask=mask)
  CALL MPI_ALLREDUCE(arr_min, parallel_masked_min, 1, MPI_REAL, MPI_MIN, MPI_COMM_WORLD, mpi_err)
END FUNCTION parallel_masked_min
!===============================================================
FUNCTION parallel_masked_sum (arr, mask)
! Implicit variables are evil
  IMPLICIT NONE
! Inputs
  REAL(MyR), DIMENSION(0:, 0:, 0:, 0:), INTENT(IN)  ::  arr
  LOGICAL,   DIMENSION(0:, 0:, 0:, 0:), INTENT(IN)  ::  mask
! Outputs
  REAL(4)  ::  parallel_masked_sum
! Local
  REAL(4)  ::  arr_sum
  INTEGER    ::  mpi_err

  arr_sum = SUM(arr, mask=mask)
  CALL MPI_ALLREDUCE(arr_sum, parallel_masked_sum, 1, MPI_REAL, MPI_SUM, MPI_COMM_WORLD, mpi_err)
END FUNCTION parallel_masked_sum
!===============================================================
FUNCTION parallel_mean (arr)
! Implicit variables are evil
  IMPLICIT NONE
! Inputs
  REAL(MyR), DIMENSION(0:, 0:, 0:, 0:), INTENT(IN)  ::  arr
! Outputs
  REAL(MyR)  ::  parallel_mean
! Local
  REAL(8)     ::  local_sum, total_sum
  REAL(8)     ::  local_num, total_num
  INTEGER(4)  ::  mpi_err
  INTEGER(4), DIMENSION(4)  ::  dims
  INTEGER(4)  ::  x, y, z, t
  INTEGER(4)  ::  nx, ny, nz, nt
  
  dims = shape(arr)
  nx = dims(1) - 2 * gc
  ny = dims(2) - 2 * gc
  nz = dims(3) - 2 * gc
  nt = dims(4) - 2 * gc

  local_sum = 0.0
  local_num = 0

    DO t = 1, nt
      DO z = 1, nz
        DO y = 1, ny
          DO x = 1, nx
          local_sum = local_sum + arr(x,y,z,t)
          local_num = local_num + 1
        END DO
      END DO
    END DO
  END DO

  CALL MPI_ALLREDUCE(local_sum, total_sum, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, mpi_err)
  CALL MPI_ALLREDUCE(local_num, total_num, 1, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD, mpi_err)
  
  parallel_mean = REAL(total_sum / (REAL(total_num, kind=8) + 1E-6_dbl), kind=4)
END FUNCTION parallel_mean
!===============================================================
FUNCTION parallel_max (arr)
! Implicit variables are evil
  IMPLICIT NONE
! Inputs
  REAL(MyR), DIMENSION(0:, 0:, 0:, 0:), INTENT(IN)  ::  arr
! Outputs
  REAL(4)  ::  parallel_max
! Local
  REAL(4)  ::  arr_max
  INTEGER    ::  mpi_err

  arr_max = MAXVAL(arr)
  CALL MPI_ALLREDUCE(arr_max, parallel_max, 1, MPI_REAL, MPI_MAX, MPI_COMM_WORLD, mpi_err)
END FUNCTION parallel_max
!===============================================================
FUNCTION parallel_min (arr)
! Implicit variables are evil
  IMPLICIT NONE
! Inputs
  REAL(MyR), DIMENSION(0:, 0:, 0:, 0:), INTENT(IN)  ::  arr
! Outputs
  REAL(4)  ::  parallel_min
! Local
  REAL(4)  ::  arr_min
  INTEGER    ::  mpi_err

  arr_min = MINVAL(arr)
  CALL MPI_ALLREDUCE(arr_min, parallel_min, 1, MPI_REAL, MPI_MIN, MPI_COMM_WORLD, mpi_err)
END FUNCTION parallel_min
!===============================================================
FUNCTION parallel_sum (arr)
! Implicit variables are evil
  IMPLICIT NONE
! Inputs
  REAL(MyR), DIMENSION(0:, 0:, 0:, 0:), INTENT(IN)  ::  arr
! Outputs
  REAL(4)  ::  parallel_sum
! Local
  REAL(4)  ::  arr_sum
  INTEGER    ::  mpi_err

  arr_sum = SUM(arr)
  CALL MPI_ALLREDUCE(arr_sum, parallel_sum, 1, MPI_REAL, MPI_SUM, MPI_COMM_WORLD, mpi_err)
END FUNCTION parallel_sum
!===============================================================


END MODULE PSCS_4D_parallel_collective_ops
