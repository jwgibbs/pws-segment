MODULE PSCS_4D_parallel_BCs

USE PSCS_constants
USE PSCS_4D_parallel_proc_split
IMPLICIT NONE

CONTAINS

SUBROUTINE const_slope_BC ( arr )
!===============================================================
!  -IN ALL (X, Y AND Z) DIRECTIONS, MIRROR EDGE VALUES TO FILL
!   GHOST CELL VALUES.
!  -USE MPI TO PASS DATA BETWEEN PROCESSORS IN 12 STEPS:
!     1) X-DIRECTION, LOW TO HIGH, EVEN SENDS AND ODD RECEIVES
!     2) X-DIRECTION, LOW TO HIGH, ODD SENDS AND EVEN RECEIVES
!     3) X-DIRECTION, HIGH TO LOW, EVEN SENDS AND ODD RECEIVES
!     4) X-DIRECTION, HIGH TO LOW, ODD SENDS AND EVEN RECEIVES
!
!     5) Y-DIRECTION, LOW TO HIGH, EVEN SENDS AND ODD RECEIVES
!     6) Y-DIRECTION, LOW TO HIGH, ODD SENDS AND EVEN RECEIVES
!     7) Y-DIRECTION, HIGH TO LOW, EVEN SENDS AND ODD RECEIVES
!     8) Y-DIRECTION, HIGH TO LOW, ODD SENDS AND EVEN RECEIVES
!
!     9) Z-DIRECTION, LOW TO HIGH, EVEN SENDS AND ODD RECEIVES
!    10) Z-DIRECTION, LOW TO HIGH, ODD SENDS AND EVEN RECEIVES
!    11) Z-DIRECTION, HIGH TO LOW, EVEN SENDS AND ODD RECEIVES
!    12) Z-DIRECTION, HIGH TO LOW, ODD SENDS AND EVEN RECEIVES

!    13) Z-DIRECTION, LOW TO HIGH, EVEN SENDS AND ODD RECEIVES
!    14) Z-DIRECTION, LOW TO HIGH, ODD SENDS AND EVEN RECEIVES
!    15) Z-DIRECTION, HIGH TO LOW, EVEN SENDS AND ODD RECEIVES
!    16) Z-DIRECTION, HIGH TO LOW, ODD SENDS AND EVEN RECEIVES

!===============================================================
IMPLICIT NONE
!===========================================================
REAL(kind=MyR), DIMENSION(0:, 0:, 0:, 0:), TARGET, INTENT(INOUT)  ::  arr
REAL(kind=4), DIMENSION(:,:,:,:), POINTER  :: send, recv
INTEGER, DIMENSION(np)  :: iStatus
INTEGER                 :: i, mpi_err, tag, destProc, sourceProc, size
INTEGER  ::  id_x, id_y, id_z, id_t
INTEGER  ::  np_x, np_y, np_z, np_t
INTEGER  ::  nx, ny, nz, nt
LOGICAL  ::  x_not_first, y_not_first, z_not_first, t_not_first
LOGICAL  ::  x_not_last, y_not_last, z_not_last, t_not_last
LOGICAL  ::  x_even, y_even, z_even, t_even
LOGICAL  ::  x_odd, y_odd, z_odd, t_odd
INTEGER, DIMENSION(4) :: dims, proc_id, send_shape
!===========================================================
    CALL MPI_BARRIER (MPI_COMM_WORLD, mpi_err )
    !===========================================================
    dims = shape(arr)
    nx = dims(1) - 2 * gc
    ny = dims(2) - 2 * gc
    nz = dims(3) - 2 * gc
    nt = dims(4) - 2 * gc
    
    proc_id = rank_to_xyzt(rank)
    id_x = proc_id(1)
    id_y = proc_id(2)
    id_z = proc_id(3)
    id_t = proc_id(4)
    
    np_x = np_i(1)
    np_y = np_i(2)
    np_z = np_i(3)
    np_t = np_i(4)
    
    x_even = (mod(id_x,2) == 0)
    y_even = (mod(id_y,2) == 0)
    z_even = (mod(id_z,2) == 0)
    t_even = (mod(id_t,2) == 0)
    
    x_odd = (mod(id_x,2) == 1)
    y_odd = (mod(id_y,2) == 1)
    z_odd = (mod(id_z,2) == 1)
    t_odd = (mod(id_t,2) == 1)
    
    x_not_first = (id_x /= 0)
    y_not_first = (id_y /= 0)
    z_not_first = (id_z /= 0)
    t_not_first = (id_t /= 0)
    
    x_not_last = (id_x /= np_x - 1)
    y_not_last = (id_y /= np_y - 1)
    z_not_last = (id_z /= np_z - 1)
    t_not_last = (id_t /= np_t - 1)
    
    DO i = 1,gc
      IF (id_X .EQ.      0) arr(+1-i, :, :, :) = +2 * arr(+1+0, :, :, :) - 1 * arr(+1+i, :, :, :)
      IF (id_X .EQ. np_X-1) arr(nx+i, :, :, :) = +2 * arr(nx+0, :, :, :) - 1 * arr(nx-i, :, :, :)

      IF (id_Y .EQ.      0) arr(:, +1-i, :, :) = +2 * arr(:, +1+0, :, :) - 1 * arr(:, +1+i, :, :)
      IF (id_Y .EQ. np_Y-1) arr(:, ny+i, :, :) = +2 * arr(:, ny+0, :, :) - 1 * arr(:, ny-i, :, :)

      IF (id_Z .EQ.      0) arr(:, :, +1-i, :) = +2 * arr(:, :, +1+0, :) - 1 * arr(:, :, +1+i, :)
      IF (id_Z .EQ. np_Z-1) arr(:, :, nz+i, :) = +2 * arr(:, :, nz+0, :) - 1 * arr(:, :, nz-i, :)

      IF (id_t .EQ.      0) arr(:, :, :, +1-i) = +2 * arr(:, :, :, +1+0) - 1 * arr(:, :, :, +1+i)
      IF (id_t .EQ. np_t-1) arr(:, :, :, nt+i) = +2 * arr(:, :, :, nt+0) - 1 * arr(:, :, :, nt-i)
    END DO

!===============================================================
! STEP 1: X-DIRECTION, LOW TO HIGH, EVEN SENDS AND ODD RECEIVES
  tag = 1
  size = (ny+2*gc) * (nz+2*gc) * (nt+2*gc) * gc
  sourceProc = xyzt_to_rank(id_X - 1, id_Y, id_Z, id_t)
  destProc   = xyzt_to_rank(id_X + 1, id_Y, id_Z, id_t)
  send => arr(nx+1-gc:nx, :, :, :)
  recv => arr( 0+1-gc: 0, :, :, :)

  IF ( x_not_last .AND. x_even ) THEN
      !PRINT *, rank, ' sending to ', destProc
      CALL MPI_Send ( send, size, MPI_REAL, destProc, tag, MPI_COMM_WORLD, mpi_err )
  ELSE IF ( x_not_first .AND. x_odd ) THEN
      !PRINT *, rank, ' receiving from ', sourceProc
      CALL MPI_Recv ( recv, size, MPI_REAL, sourceProc, tag, MPI_COMM_WORLD, iStatus, mpi_err )
  END IF
  !print *, rank, istatus, mpi_err
  CALL MPI_BARRIER (MPI_COMM_WORLD, mpi_err )
!===============================================================
! STEP 2: X-DIRECTION, LOW TO HIGH, ODD SENDS AND EVEN RECEIVES
  tag = 2 ! EVERYTHING ELSE STAYS THE SAME
  
  IF ( x_not_last .AND. x_odd ) THEN
      !PRINT *, rank, ' sending to ', destProc
      CALL MPI_Send ( send, size, MPI_REAL, destProc, tag, MPI_COMM_WORLD, mpi_err )
  ELSE IF ( x_not_first .AND. x_even ) THEN
      !PRINT *, rank, ' receiving from ', sourceProc
      CALL MPI_Recv ( recv, size, MPI_REAL, sourceProc, tag, MPI_COMM_WORLD, iStatus, mpi_err )
  END IF
  CALL MPI_BARRIER (MPI_COMM_WORLD, mpi_err )
!===============================================================
! STEP 3: X-DIRECTION, HIGH TO LOW, EVEN SENDS AND ODD RECEIVES
  tag = 3
  size = (ny+2*gc) * (nz+2*gc) * (nt+2*gc) * gc
  sourceProc = xyzt_to_rank(id_X + 1, id_Y, id_Z, id_t)
  destProc   = xyzt_to_rank(id_X - 1, id_Y, id_Z, id_t)
  send => arr( 0+1: 0+gc, :, :, :)
  recv => arr(nx+1:nx+gc, :, :, :)
  
  IF ( x_not_first .AND. x_even ) THEN
      CALL MPI_Send ( send, size, MPI_REAL, destProc, tag, MPI_COMM_WORLD, mpi_err )
  ELSE IF ( x_not_last .AND. x_odd ) THEN
      CALL MPI_Recv ( recv, size, MPI_REAL, sourceProc, tag, MPI_COMM_WORLD, iStatus, mpi_err )
  END IF
  
  CALL MPI_BARRIER (MPI_COMM_WORLD, mpi_err )
!===============================================================
! STEP 4: X-DIRECTION, HIGH TO LOW, ODD SENDS AND EVEN RECEIVES
  tag = 4 ! EVERYTHING ELSE STAYS THE SAME
  
  IF ( x_not_first .AND. x_odd ) THEN
      CALL MPI_Send ( send, size, MPI_REAL, destProc, tag, MPI_COMM_WORLD, mpi_err )
  ELSE IF ( x_not_last .AND. x_even ) THEN
      CALL MPI_Recv ( recv, size, MPI_REAL, sourceProc, tag, MPI_COMM_WORLD, iStatus, mpi_err )
  END IF
  
  CALL MPI_BARRIER (MPI_COMM_WORLD, mpi_err )
!===============================================================
! STEP 5: Y-DIRECTION, LOW TO HIGH, EVEN SENDS AND ODD RECEIVES
  tag = 5
  size = (nx+2*gc) * (nz+2*gc) * (nt+2*gc) * gc
  sourceProc = xyzt_to_rank(id_X, id_Y - 1, id_Z, id_t)
  destProc   = xyzt_to_rank(id_X, id_Y + 1, id_Z, id_t)
  send => arr(:, ny+1-gc:ny, :, :)
  recv => arr(:,  0+1-gc: 0, :, :)
  
  IF      ( y_not_last .AND. y_even ) THEN
      CALL MPI_Send ( send, size, MPI_REAL, destProc, tag, MPI_COMM_WORLD, mpi_err )
  ELSE IF ( y_not_first .AND. y_odd ) THEN
      CALL MPI_Recv ( recv, size, MPI_REAL, sourceProc, tag, MPI_COMM_WORLD, iStatus, mpi_err )
  END IF
  
  CALL MPI_BARRIER (MPI_COMM_WORLD, mpi_err )
!===============================================================
! STEP 6: Y-DIRECTION, LOW TO HIGH, ODD SENDS AND EVEN RECEIVES
  tag = 6 ! EVERYTHING ELSE STAYS THE SAME

  IF      ( y_not_last .AND. y_odd ) THEN
      CALL MPI_Send ( send, size, MPI_REAL, destProc, tag, MPI_COMM_WORLD, mpi_err )
  ELSE IF ( y_not_first .AND. y_even ) THEN
      CALL MPI_Recv ( recv, size, MPI_REAL, sourceProc, tag, MPI_COMM_WORLD, iStatus, mpi_err )
  END IF

  CALL MPI_BARRIER (MPI_COMM_WORLD, mpi_err )
!===============================================================
! STEP 7: Y-DIRECTION, HIGH TO LOW, EVEN SENDS AND ODD RECEIVES
  tag = 7
  size = (nx+2*gc) * (nz+2*gc) * (nt+2*gc) * gc
  sourceProc = xyzt_to_rank(id_X, id_Y + 1, id_Z, id_t)
  destProc   = xyzt_to_rank(id_X, id_Y - 1, id_Z, id_t)
  send => arr(:,  0+1: 0+gc, :, :)
  recv => arr(:, ny+1:ny+gc, :, :)

  IF ( y_not_first .AND. y_even ) THEN
      CALL MPI_Send ( send, size, MPI_REAL, destProc, tag, MPI_COMM_WORLD, mpi_err )
  ELSE IF ( y_not_last .AND. y_odd ) THEN
      CALL MPI_Recv ( recv, size, MPI_REAL, sourceProc, tag, MPI_COMM_WORLD, iStatus, mpi_err )
  END IF

  CALL MPI_BARRIER (MPI_COMM_WORLD, mpi_err )
!===============================================================
! STEP 8: Y-DIRECTION, HIGH TO LOW, ODD SENDS AND EVEN RECEIVES
  tag = 8 ! EVERYTHING ELSE STAYS THE SAME
  
  IF ( y_not_first .AND. y_odd ) THEN
      CALL MPI_Send ( send, size, MPI_REAL, destProc, tag, MPI_COMM_WORLD, mpi_err )
  ELSE IF ( y_not_last .AND. y_even ) THEN
      CALL MPI_Recv ( recv, size, MPI_REAL, sourceProc, tag, MPI_COMM_WORLD, iStatus, mpi_err )
  END IF

  CALL MPI_BARRIER (MPI_COMM_WORLD, mpi_err )
!===============================================================
! STEP 9: Z-DIRECTION, LOW TO HIGH, EVEN SENDS AND ODD RECEIVES
  tag = 9
  size = (nx+2*gc) * (ny+2*gc) * (nt+2*gc) * gc
  sourceProc = xyzt_to_rank(id_X, id_Y, id_Z - 1, id_t)
  destProc   = xyzt_to_rank(id_X, id_Y, id_Z + 1, id_t)
  send => arr(:, :, nz+1-gc:nz, :)
  recv => arr(:, :,  0+1-gc: 0, :)

  IF ( z_not_last .AND. z_even ) THEN
      CALL MPI_Send ( send, size, MPI_REAL, destProc, tag, MPI_COMM_WORLD, mpi_err )
  ELSE IF ( z_not_first .AND. z_odd ) THEN
      CALL MPI_Recv ( recv, size, MPI_REAL, sourceProc, tag, MPI_COMM_WORLD, iStatus, mpi_err )
  END IF

  CALL MPI_BARRIER (MPI_COMM_WORLD, mpi_err )
!===============================================================
! STEP 10: Z-DIRECTION, LOW TO HIGH, ODD SENDS AND EVEN RECEIVES
  tag = 10 ! EVERYTHING ELSE STAYS THE SAME
  
  IF      ( z_not_last .AND. z_odd ) THEN
      CALL MPI_Send ( send, size, MPI_REAL, destProc, tag, MPI_COMM_WORLD, mpi_err )
  ELSE IF ( z_not_first .AND. z_even ) THEN
     CALL MPI_Recv ( recv, size, MPI_REAL, sourceProc, tag, MPI_COMM_WORLD, iStatus, mpi_err )
  END IF

  CALL MPI_BARRIER (MPI_COMM_WORLD, mpi_err )
!===============================================================
! STEP 11: Z-DIRECTION, HIGH TO LOW, EVEN SENDS AND ODD RECEIVES
  tag = 11
  size = (nx+2*gc) * (ny+2*gc) * (nt+2*gc) * gc
  sourceProc = xyzt_to_rank(id_X, id_Y, id_Z + 1, id_t)
  destProc   = xyzt_to_rank(id_X, id_Y, id_Z - 1, id_t)
  send => arr(:, :,  0+1: 0+gc, :)
  recv => arr(:, :, nz+1:nz+gc, :)

  IF      ( z_not_first .AND. z_even ) THEN
      CALL MPI_Send ( send, size, MPI_REAL, destProc, tag, MPI_COMM_WORLD, mpi_err )
  ELSE IF ( z_not_last .AND. z_odd ) THEN
      CALL MPI_Recv ( recv, size, MPI_REAL, sourceProc, tag, MPI_COMM_WORLD, iStatus, mpi_err )
  END IF

  CALL MPI_BARRIER (MPI_COMM_WORLD, mpi_err )
!===============================================================
! STEP 12: Z-DIRECTION, HIGH TO LOW, ODD SENDS AND EVEN RECEIVES
  tag = 12 ! EVERYTHING ELSE STAYS THE SAME
  
  IF      ( z_not_first .AND. z_odd ) THEN
      CALL MPI_Send ( send, size, MPI_REAL, destProc, tag, MPI_COMM_WORLD, mpi_err )
  ELSE IF ( z_not_last .AND. z_even ) THEN
      CALL MPI_Recv ( recv, size, MPI_REAL, sourceProc, tag, MPI_COMM_WORLD, iStatus, mpi_err )
  END IF

  CALL MPI_BARRIER (MPI_COMM_WORLD, mpi_err )
!===============================================================
! STEP 13: t-DIRECTION, LOW TO HIGH, EVEN SENDS AND ODD RECEIVES
  tag = 13
  size = (nx+2*gc) * (ny+2*gc) * (nz+2*gc) * gc
  sourceProc = xyzt_to_rank(id_X, id_Y, id_Z, id_t - 1)
  destProc   = xyzt_to_rank(id_X, id_Y, id_Z, id_t + 1)
  send => arr(:, :, :, nt+1-gc:nt)
  recv => arr(:, :, :,  0+1-gc: 0)

  IF ( t_not_last .AND. t_even ) THEN
      CALL MPI_Send ( send, size, MPI_REAL, destProc, tag, MPI_COMM_WORLD, mpi_err )
  ELSE IF ( t_not_first .AND. t_odd ) THEN
      CALL MPI_Recv ( recv, size, MPI_REAL, sourceProc, tag, MPI_COMM_WORLD, iStatus, mpi_err )
  END IF

  CALL MPI_BARRIER (MPI_COMM_WORLD, mpi_err )
!===============================================================
! STEP 14: t-DIRECTION, LOW TO HIGH, ODD SENDS AND EVEN RECEIVES
  tag = 14 ! EVERYTHING ELSE STAYS THE SAME
  
  IF ( t_not_last .AND. t_odd ) THEN
      CALL MPI_Send ( send, size, MPI_REAL, destProc, tag, MPI_COMM_WORLD, mpi_err )
  ELSE IF ( t_not_first .AND. t_even ) THEN
     CALL MPI_Recv ( recv, size, MPI_REAL, sourceProc, tag, MPI_COMM_WORLD, iStatus, mpi_err )
  END IF

  CALL MPI_BARRIER (MPI_COMM_WORLD, mpi_err )
!===============================================================
! STEP 15: t-DIRECTION, HIGH TO LOW, EVEN SENDS AND ODD RECEIVES
  tag = 15
  size = (nx+2*gc) * (ny+2*gc) * (nz+2*gc) * gc
  sourceProc = xyzt_to_rank(id_X, id_Y, id_Z, id_t + 1)
  destProc   = xyzt_to_rank(id_X, id_Y, id_Z, id_t - 1)
  send => arr(:, :, :,  0+1: 0+gc)
  recv => arr(:, :, :, nt+1:nt+gc)

  IF ( t_not_first .AND. t_even ) THEN
      CALL MPI_Send ( send, size, MPI_REAL, destProc, tag, MPI_COMM_WORLD, mpi_err )
  ELSE IF ( t_not_last .AND. t_odd ) THEN
      CALL MPI_Recv ( recv, size, MPI_REAL, sourceProc, tag, MPI_COMM_WORLD, iStatus, mpi_err )
  END IF

  CALL MPI_BARRIER (MPI_COMM_WORLD, mpi_err )
!===============================================================
! STEP 16: t-DIRECTION, HIGH TO LOW, ODD SENDS AND EVEN RECEIVES
  tag = 16 ! EVERYTHING ELSE STAYS THE SAME
  
  IF ( t_not_first .AND. t_odd ) THEN
      CALL MPI_Send ( send, size, MPI_REAL, destProc, tag, MPI_COMM_WORLD, mpi_err )
  ELSE IF ( t_not_last .AND. t_even ) THEN
      CALL MPI_Recv ( recv, size, MPI_REAL, sourceProc, tag, MPI_COMM_WORLD, iStatus, mpi_err )
  END IF

  CALL MPI_BARRIER (MPI_COMM_WORLD, mpi_err )
!===============================================================

    NULLIFY (send, recv)
    CALL MPI_BARRIER (MPI_COMM_WORLD, mpi_err )
!===============================================================

END SUBROUTINE const_slope_BC

END MODULE PSCS_4D_parallel_BCs
