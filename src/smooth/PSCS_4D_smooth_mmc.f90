MODULE PSCS_4D_smooth_mmc
! =============================================================
USE PSCS_4D_parallel
USE PSCS_constants
IMPLICIT NONE
! =============================================================
CONTAINS

FUNCTION mmc ( arr, iters, dt )
! ==============================================================
IMPLICIT NONE

! Inputs:
REAL(kind=MyR), DIMENSION(0:, 0:, 0:, 0:), INTENT(IN)   :: arr
REAL, INTENT(IN)  ::  dt
INTEGER, INTENT(IN)  ::  iters

! Outputs:
REAL(kind=MyR), DIMENSION(:,:,:,:), ALLOCATABLE  :: mmc

! In/out:

! Local only:
REAL(kind=MyR), DIMENSION(:,:,:,:), ALLOCATABLE  :: mmc_t
INTEGER  ::  mpi_err
INTEGER  ::  iter
INTEGER  ::  x, y, z, t
INTEGER  ::  nx, ny, nz, nt
INTEGER, DIMENSION(4) :: dims
REAL(kind=MyR)  :: grad
REAL(kind=MyR)  :: grad_x, grad_y, grad_z
REAL(kind=MyR)  :: grad_xx, grad_yy, grad_zz
REAL(kind=MyR)  :: grad_xy, grad_xz, grad_yz
! ==============================================================
  !WRITE (*,'( 2X, A )') "MOTION BY MEAN CURVATURE SMOOTHING"
  
  dims = shape(arr)
  nx = dims(1) - 2 * gc
  ny = dims(2) - 2 * gc
  nz = dims(3) - 2 * gc
  nt = dims(4) - 2 * gc
  
  ALLOCATE ( mmc_t ( 0:nx+1, 0:ny+1, 0:nz+1, 0:nt+1 ) )
  ALLOCATE (   mmc ( 0:nx+1, 0:ny+1, 0:nz+1, 0:nt+1 ) )
  
  mmc = arr
  mmc_t = 0.0
  ! ============================================================
  DO iter = 1,iters
  
    DO t = 1, nt
      DO z = 1, nz
        DO y = 1, ny
          DO x = 1, nx
            grad_X = ( -1.0 * mmc(X-1, Y, Z, t) &
                       +1.0 * mmc(X+1, Y, Z, t) ) / 2.0

            grad_Y = ( -1.0 * mmc(X, Y-1, Z, t) &
                       +1.0 * mmc(X, Y+1, Z, t) ) / 2.0

            grad_Z = ( -1.0 * mmc(X, Y, Z-1, t) &
                       +1.0 * mmc(X, Y, Z+1, t) ) / 2.0

            grad_XX = ( + 1.0 * mmc(X-1, Y, Z, t) &
                        - 2.0 * mmc(X+0, Y, Z, t) &
                        + 1.0 * mmc(X+1, Y, Z, t) ) / 1.0

            grad_YY = ( + 1.0 * mmc(X, Y-1, Z, t) &
                        - 2.0 * mmc(X, Y+0, Z, t) &
                        + 1.0 * mmc(X, Y+1, Z, t) ) / 1.0

            grad_ZZ = ( + 1.0 * mmc(X, Y, Z-1, t) &
                        - 2.0 * mmc(X, Y, Z+0, t) &
                        + 1.0 * mmc(X, Y, Z+1, t) ) / 1.0

            grad_XY = ( + 1.0 * mmc(X-1, Y-1, Z, t) &
                        - 1.0 * mmc(X-1, Y+1, Z, t) &
                        - 1.0 * mmc(X+1, Y-1, Z, t) &
                        + 1.0 * mmc(X+1, Y+1, Z, t) ) / 4.0

            grad_XZ = ( + 1.0 * mmc(X-1, Y, Z-1, t) &
                        - 1.0 * mmc(X-1, Y, Z+1, t) &
                        - 1.0 * mmc(X+1, Y, Z-1, t) &
                        + 1.0 * mmc(X+1, Y, Z+1, t) ) / 4.0

            grad_YZ = ( + 1.0 * mmc(X, Y-1, Z-1, t) &
                        - 1.0 * mmc(X, Y-1, Z+1, t) &
                        - 1.0 * mmc(X, Y+1, Z-1, t) &
                        + 1.0 * mmc(X, Y+1, Z+1, t) ) / 4.0
          
            grad = SQRT(grad_x**2 + grad_y**2 + grad_z**2)
          
            IF (grad > 1e-2) THEN
              mmc_t(x,y,z,t) = ( + 1.0 * grad_x**2 * grad_yy + grad_x**2 * grad_zz &
                                 + 1.0 * grad_y**2 * grad_xx + grad_y**2 * grad_zz &
                                 + 1.0 * grad_z**2 * grad_xx + grad_z**2 * grad_yy &
                                 - 2.0 * grad_x * grad_y * grad_xy &
                                 - 2.0 * grad_x * grad_z * grad_xz &
                                 - 2.0 * grad_y * grad_z * grad_yz ) &
                                  / (2.0 * grad**3 + 1e-2)
            ELSE
              mmc_t(x,y,z,t) = 0.0
            END IF
            
          END DO
        END DO
      END DO
    END DO
    
    mmc = mmc + dt * mmc_t
  
    CALL const_slope_BC(mmc)
  
  END DO
  ! ============================================================
  DEALLOCATE( mmc_t )
  
END FUNCTION mmc

END MODULE PSCS_4D_smooth_mmc
