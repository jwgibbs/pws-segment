MODULE PSCS_4D_smooth_ds
! =============================================================
USE PSCS_4D_parallel
USE PSCS_constants
IMPLICIT NONE
! =============================================================
CONTAINS

FUNCTION ds ( arr, iters, dt, D )
! ==============================================================
IMPLICIT NONE

! Inputs:
  REAL(kind=MyR), DIMENSION(0:, 0:, 0:, 0:), INTENT(IN)   :: arr
  REAL, INTENT(IN)  ::  dt
  INTEGER, INTENT(IN)  ::  iters
  REAL(kind=MyR), DIMENSION(4)  ::  D

! Outputs:
  REAL(kind=MyR), DIMENSION(:,:,:,:), ALLOCATABLE  :: ds

! In/out:

! Local only:
  REAL(kind=MyR), DIMENSION(:,:,:,:), ALLOCATABLE  :: ds_t
  INTEGER  ::  mpi_err
  INTEGER  ::  iter
  INTEGER  ::  x, y, z, t
  INTEGER  ::  nx, ny, nz, nt
  INTEGER, DIMENSION(4) :: dims
  REAL(kind=MyR)  :: grad_x, grad_y, grad_z, grad_t
! ==============================================================
  !WRITE (*,'( 2X, A )') "DIFFUSION SMOOTHING"
  
  dims = shape(arr)
  nx = dims(1) - 2 * gc
  ny = dims(2) - 2 * gc
  nz = dims(3) - 2 * gc
  nt = dims(4) - 2 * gc
  
  ALLOCATE ( ds_t ( 0:nx+1, 0:ny+1, 0:nz+1, 0:nt+1 ) )
  ALLOCATE (   ds ( 0:nx+1, 0:ny+1, 0:nz+1, 0:nt+1 ) )
  
  ds = arr
  ds_t = 0.0
  ! ============================================================
  DO iter = 1,iters
    
    DO t = 1, nt
      DO z = 1, nz
        DO y = 1, ny
          DO x = 1, nx
            grad_x = ds(x+1,   y,   z,   t) - 2 * ds(  x,   y,   z,   t) +  ds(x-1,   y,   z,   t)
            grad_y = ds(  x, y+1,   z,   t) - 2 * ds(  x,   y,   z,   t) +  ds(  x, y-1,   z,   t)
            grad_z = ds(  x,   y, z+1,   t) - 2 * ds(  x,   y,   z,   t) +  ds(  x,   y, z-1,   t)
            grad_t = ds(  x,   y,   z, t+1) - 2 * ds(  x,   y,   z,   t) +  ds(  x,   y,   z, t-1)
          
            ds_t(X,Y,Z,t) = + D(1) * grad_x &
                            + D(2) * grad_y &
                            + D(3) * grad_z &
                            + D(4) * grad_t
          END DO
        END DO
      END DO
    END DO
    
    ds = ds + dt * ds_t
    
    CALL const_slope_BC(ds)
    
  END DO
  ! ============================================================
  DEALLOCATE( ds_t )
  CALL MPI_BARRIER (MPI_COMM_WORLD, mpi_err )
  
END FUNCTION ds

END MODULE PSCS_4D_smooth_ds

