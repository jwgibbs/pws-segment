MODULE PSCS_3D_smooth_ds
! =============================================================
USE PSCS_3D_parallel
USE PSCS_constants
IMPLICIT NONE
! =============================================================
CONTAINS

FUNCTION ds ( arr, iters, dt )
! ==============================================================
IMPLICIT NONE

! Inputs:
  REAL(kind=MyR), DIMENSION(0:, 0:, 0:), INTENT(IN)   :: arr
  REAL, INTENT(IN)  ::  dt
  INTEGER, INTENT(IN)  ::  iters

! Outputs:
  REAL(kind=MyR), DIMENSION(:,:,:), ALLOCATABLE  :: ds

! In/out:

! Local only:
  REAL(kind=MyR), DIMENSION(:,:,:), ALLOCATABLE  :: ds_t
  INTEGER  ::  mpi_err
  INTEGER  ::  iter
  INTEGER  ::  x, y, z
  INTEGER  ::  nx, ny, nz
  INTEGER, DIMENSION(3) :: dims
  REAL(kind=MyR)  :: grad_x, grad_y, grad_z
! ==============================================================
  !WRITE (*,'( 2X, A )') "DIFFUSION SMOOTHING"
  
  dims = shape(arr)
  nx = dims(1) - 2 * gc
  ny = dims(2) - 2 * gc
  nz = dims(3) - 2 * gc
  
  ALLOCATE ( ds_t ( 0:nx+1, 0:ny+1, 0:nz+1 ) )
  ALLOCATE (   ds ( 0:nx+1, 0:ny+1, 0:nz+1 ) )
  
  ds = arr
  ds_t = 0.0
  ! ============================================================
  DO iter = 1,iters
    
    DO z = 1, nz
      DO y = 1, ny
        DO x = 1, nx
          grad_x = ds(x+1,   y,   z) - 2 * ds(  x,   y,   z) +  ds(x-1,   y,   z)
          grad_y = ds(  x, y+1,   z) - 2 * ds(  x,   y,   z) +  ds(  x, y-1,   z)
          grad_z = ds(  x,   y, z+1) - 2 * ds(  x,   y,   z) +  ds(  x,   y, z-1)
          
          ds_t(X,Y,Z) = grad_x + grad_y + grad_z
        END DO
      END DO
    END DO
    
    ds = ds + dt * ds_t
    
    CALL const_slope_BC(ds)
    
  END DO
  ! ============================================================
  DEALLOCATE( ds_t )
  CALL MPI_BARRIER (MPI_COMM_WORLD, mpi_err )
  
END FUNCTION ds

END MODULE PSCS_3D_smooth_ds

