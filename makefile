# === GLOBAL OPTIONS ===========================================
# Fortran compiler:
export FC=h5pfc

# Compiling:
export GLO_WARNINGS = -Wall -pedantic -Wno-unused-variable
export GLO_OPTIMIZE = -O3 -march=corei7
export GLO_INC_DIRS = 

# Linking:
export GLO_LINK_DIRS = 
export GLO_LINK_LIBS = 

# === LOCAL OPTIONS ============================================
# Directories:
SRC_DIR = ./src
OBJ_DIR = ./include
LIB_DIR = ./lib

# Compiling:
WARNINGS = $(GLO_WARNINGS)
OPTIMIZE = $(GLO_OPTIMIZE)
INC_DIRS = $(GLO_INC_DIRS) -I$(OBJ_DIR)
FC_FLAGS = $(OPTIMIZE) $(WARNINGS) $(INC_DIRS) -J$(OBJ_DIR)

# Linking:
LINK_DIRS = $(GLO_LINK_DIRS) -L$(LIB_DIR)
LINK_LIBS = $(GLO_LINK_LIBS) -lpscs_io -lpscs_parallel -lpscs_sdf -lpscs_smooth
LINK_FLAGS = $(LINK_DIRS) $(LINK_LIBS)

# === Collective Objects =======================================================
CO_const = $(OBJ_DIR)/PSCS_constants.o
CO_io = $(OBJ_DIR)/PSCS_3D_io.o $(OBJ_DIR)/PSCS_4D_io.o
CO_parallel = $(OBJ_DIR)/PSCS_3D_parallel.o $(OBJ_DIR)/PSCS_4D_parallel.o
CO_sdf = $(OBJ_DIR)/PSCS_3D_sdf.o $(OBJ_DIR)/PSCS_4D_sdf.o 
CO_smooth = $(OBJ_DIR)/PSCS_3D_smooth.o $(OBJ_DIR)/PSCS_4D_smooth.o
# === Individual Objects =======================================================
IO_const = $(OBJ_DIR)/PSCS_constants.o
IO_io = $(OBJ_DIR)/PSCS_3D_io_misc.o \
        $(OBJ_DIR)/PSCS_3D_io_read.o \
        $(OBJ_DIR)/PSCS_3D_io_write.o \
        $(OBJ_DIR)/PSCS_3D_io_read_inputs.o \
        $(OBJ_DIR)/PSCS_4D_io_misc.o \
        $(OBJ_DIR)/PSCS_4D_io_read.o \
        $(OBJ_DIR)/PSCS_4D_io_write.o \
        $(OBJ_DIR)/PSCS_4D_io_read_inputs.o
IO_parallel = $(OBJ_DIR)/PSCS_3D_parallel_BCs.o \
              $(OBJ_DIR)/PSCS_3D_parallel_collective_ops.o \
              $(OBJ_DIR)/PSCS_3D_parallel_proc_split.o\
              $(OBJ_DIR)/PSCS_4D_parallel_BCs.o \
              $(OBJ_DIR)/PSCS_4D_parallel_collective_ops.o \
              $(OBJ_DIR)/PSCS_4D_parallel_proc_split.o
IO_sdf = $(OBJ_DIR)/PSCS_3D_reinit.o \
         $(OBJ_DIR)/PSCS_3D_subcell.o \
         $(OBJ_DIR)/PSCS_4D_sdf.o 
IO_smooth = $(OBJ_DIR)/PSCS_3D_smooth_ds.o \
            $(OBJ_DIR)/PSCS_3D_smooth_mmc.o \
            $(OBJ_DIR)/PSCS_4D_smooth_ds.o \
            $(OBJ_DIR)/PSCS_4D_smooth_mmc.o
# === MAIN PROGRAM =============================================
all: $(LIB_DIR)/libpscs.a $(OBJ_DIR)/pscs_3D.o $(OBJ_DIR)/pscs_4D.o

$(LIB_DIR)/libpscs.a: $(CO_const) $(CO_io) $(CO_parallel) $(CO_sdf) $(CO_smooth) | $(LIB_DIR)
	ar r $(LIB_DIR)/libpscs.a $(IO_const) $(IO_io) $(IO_parallel) $(IO_sdf) $(IO_smooth)

# === COLLECTIVE OBJECTS =======================================================
# 3D module:
$(OBJ_DIR)/pscs_3D.o: $(SRC_DIR)/pscs_3D.f90 $(CO_const) $(CO_io) $(CO_parallel) $(CO_sdf) $(CO_smooth) | $(OBJ_DIR)
	$(FC) $(FC_FLAGS) -c $(SRC_DIR)/pscs_3D.f90 -o $(OBJ_DIR)/pscs_3D.o


# 4D module:
$(OBJ_DIR)/pscs_4D.o: $(SRC_DIR)/pscs_4D.f90 $(CO_const) $(CO_io) $(CO_parallel) $(CO_sdf) $(CO_smooth) | $(OBJ_DIR)
	$(FC) $(FC_FLAGS) -c $(SRC_DIR)/pscs_4D.f90 -o $(OBJ_DIR)/pscs_4D.o


# Constants:
$(OBJ_DIR)/PSCS_constants.o: $(SRC_DIR)/PSCS_constants.f90 | $(OBJ_DIR)
	$(FC) $(FC_FLAGS) -c $(SRC_DIR)/PSCS_constants.f90 -o $(OBJ_DIR)/PSCS_constants.o


# I/O objects:
$(OBJ_DIR)/PSCS_3D_io.o: $(SRC_DIR)/io/PSCS_3D*.f90 $(OBJ_DIR)/PSCS_constants.o $(OBJ_DIR)/PSCS_3D_parallel.o | $(OBJ_DIR)
	$(MAKE) -C $(SRC_DIR)/io -f makefile_3D

$(OBJ_DIR)/PSCS_4D_io.o: $(SRC_DIR)/io/PSCS_4D*.f90 $(OBJ_DIR)/PSCS_constants.o $(OBJ_DIR)/PSCS_4D_parallel.o | $(OBJ_DIR)
	$(MAKE) -C $(SRC_DIR)/io -f makefile_4D


# Parallel objects:
$(OBJ_DIR)/PSCS_3D_parallel.o: $(SRC_DIR)/parallel/PSCS_3D*.f90 $(OBJ_DIR)/PSCS_constants.o | $(OBJ_DIR)
	$(MAKE) -C $(SRC_DIR)/parallel -f makefile_3D

$(OBJ_DIR)/PSCS_4D_parallel.o: $(SRC_DIR)/parallel/PSCS_4D*.f90 $(OBJ_DIR)/PSCS_constants.o | $(OBJ_DIR)
	$(MAKE) -C $(SRC_DIR)/parallel -f makefile_4D


# Signed distance function objects:
$(OBJ_DIR)/PSCS_3D_sdf.o: $(SRC_DIR)/sdf/PSCS_3D*.f90 $(OBJ_DIR)/PSCS_constants.o $(CO_parallel)| $(OBJ_DIR)
	$(MAKE) -C $(SRC_DIR)/sdf -f makefile_3D

$(OBJ_DIR)/PSCS_4D_sdf.o: $(SRC_DIR)/sdf/PSCS_4D*.f90 $(OBJ_DIR)/PSCS_constants.o $(CO_parallel) | $(OBJ_DIR)
	$(MAKE) -C $(SRC_DIR)/sdf -f makefile_4D


# Smoothing objects:
$(OBJ_DIR)/PSCS_3D_smooth.o: $(SRC_DIR)/smooth/PSCS_3D*.f90 $(OBJ_DIR)/PSCS_constants.o $(CO_parallel) | $(OBJ_DIR)
	$(MAKE) -C $(SRC_DIR)/smooth -f makefile_3D

$(OBJ_DIR)/PSCS_4D_smooth.o: $(SRC_DIR)/smooth/PSCS_4D*.f90 $(OBJ_DIR)/PSCS_constants.o $(CO_parallel) | $(OBJ_DIR)
	$(MAKE) -C $(SRC_DIR)/smooth -f makefile_4D


# === MISCELLANEOUS ============================================
$(OBJ_DIR):
	mkdir -p $(OBJ_DIR)

$(LIB_DIR):
	mkdir -p $(LIB_DIR)

clean:
	rm -rf $(OBJ_DIR) $(LIB_DIR)
