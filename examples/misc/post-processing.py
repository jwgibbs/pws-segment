import im3D
import h5py
import isd
import numpy as np
import matplotlib.pyplot as plot
# === Load the data ============================================================
fn_str = '/scratch/AlCu-60-d/test/AlCu-60-d_%04i.h5'
fn_str = '/scratch/AlCu-60-d_%04i.h5'
time = 40
V_dt = 2

with h5py.File(fn_str % time, 'r') as FILE_in:
    rho = FILE_in['rho'][...]
    phi_1 = FILE_in['phi_1'][...]
    phi_2 = FILE_in['phi_2'][...]

with h5py.File(fn_str % (time-V_dt,), 'r') as FILE_in:
    V = + FILE_in['phi_2'][...]

with h5py.File(fn_str % (time+V_dt,), 'r') as FILE_in:
    V = V - FILE_in['phi_2'][...]

A = im3D.metrics.delta(phi_2)

k1 = im3D.curvature.K1(phi_2)
k2 = im3D.curvature.K2(phi_2)

H = 0.5 * (k1 + k2)

x = int(rho.shape[0] / 2.0)
y = int(rho.shape[1] / 2.0)
z = int(rho.shape[2] / 2.0)

w = np.where(phi_1 < 2.5)
phi_2[w] = +2.0
A[w] = 0.0
V[w] = 0.0
H[w] = 0.0
k1[w] = 0.0
k2[w] = 0.0

ISD_params = dict(
    bin_min = -0.15,
    bin_max = +0.15,
    nbins = 301)

ISD_A = isd.k1_k2.isd(phi_2, k1=k1, k2=k2, **ISD_params)
ISD_V = isd.k1_k2.isd(phi_2, k1=k1, k2=k2, P=V, **ISD_params)

ISD_V.data = ISD_V.data / (ISD_A.data + 1e-2)
ISD_V.data[np.where(ISD_A.data < 5)] = np.nan

cmap = plot.cm.spectral
cmap.set_bad((0.4, 0.4, 0.4))
# === Show the original data and segmented contours ============================
plot.close('all')

plot.figure(1)
plot.imshow(rho[x, :, :], vmin=+0.1, vmax=+0.8)
plot.contour(phi_1[x, :, :], levels=[0], colors='b')
plot.contour(phi_2[x, :, :], levels=[0], colors='r')

plot.figure(2)
plot.imshow(rho[:, y, :], vmin=+0.1, vmax=+0.8)
plot.contour(phi_1[:, y, :], levels=[0], colors='b')
plot.contour(phi_2[:, y, :], levels=[0], colors='r')

plot.figure(3)
plot.imshow(rho[:, :, z], vmin=+0.1, vmax=+0.8)
plot.contour(phi_1[:, :, z], levels=[0], colors='b')
plot.contour(phi_2[:, :, z], levels=[0], colors='r')
# === Plot the interfacial velocity and segmented contours =====================
plot.close('all')

plot.figure(1)
plot.imshow(V[:, :, z], vmin=-2.0, vmax=+2.0)
plot.contour(phi_1[:, :, z], levels=[0], colors='b')
plot.contour(phi_2[:, :, z], levels=[0], colors='r')
# === Plot the mean curvature and segmented contours ===========================
plot.close('all')

plot.figure(1)
plot.imshow(H[:, :, z], vmin=-2.0, vmax=+2.0)
plot.contour(phi[:, :, z], levels=[0], colors='r')
# === Save curvature and velocity data for isosurface plotting =================
H = im3D.curvature.H(phi_2)

w = np.where(phi_1 < 2.5)
phi_2[w] = +1
V[w] = 0.0
H[w] = 0.0

with h5py.File('/Users/johngibbs/desktop/test.h5', 'w') as FILE_out:
    FILE_out.create_dataset('phi', data=phi_2, dtype=np.float32, chunks=True)
    FILE_out.create_dataset('H', data=H, dtype=np.float32, chunks=True)
    FILE_out.create_dataset('V', data=V, dtype=np.float32, chunks=True)

# === Plot area ISD ============================================================
fig = plot.figure(1, figsize=(4.0, 3.5), dpi=200)
ax = ISD_A.make_axes(fig_num=1)
im = ISD_A.draw_isd(ax, cmap=cmap, interpolation='bicubic')
cb = ISD_A.add_colorbar(im, label='$P(\\kappa_1, \\kappa_2)$')

plot.savefig('/Users/johngibbs/Dropbox/AlCu-60-d_area-ISD.png', dpi=300)
# === Plot velocity ISD ========================================================
cb_min = -1.0
cb_max = +1.0

fig = plot.figure(1, figsize=(4.0, 3.5), dpi=200)
ax = ISD_V.make_axes(fig_num=1)
im = ISD_V.draw_isd(ax, cmap=cmap, vmin=cb_min, vmax=cb_max, interpolation='bicubic')
cb = ISD_V.add_colorbar(im, label='$V(\\kappa_1, \\kappa_2)$')

levels = np.array([-2.0, -1.0, +0.0, +1.0, +2.0]) * 0.25
ct_1 = ISD_V.add_contour(ax, ISD_V.data,  levels=levels, colors='k', linestyles='solid', linewidths=1)

plot.savefig('/Users/johngibbs/Dropbox/AlCu-60-d_vel-ISD.png', dpi=300)
# === Print Sv =================================================================
with h5py.File(fn_str % time, 'r') as FILE_in:
    phi = FILE_in['phi_2'][...]

A = im3D.metrics.delta(phi_2)
Sv = np.sum(A) / np.sum(phi_1 > 2.5)

print('1/Sv = %.2' % (Sv**-1,))