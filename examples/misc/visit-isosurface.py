# /Users/johngibbs/applications/visIt.app/Contents/Resources/bin/visit -movie -scriptfile visit-rotate-movie.py -output movie -format png -geometry 2048x2048
# /Users/johngibbs/applications/visIt.app/Contents/Resources/bin/visit -cli -nowin -s visit-rotate-movie.py
# visit -nowin -s viz-dendrite.py
from math import sin, cos, sqrt, pi
import os
import glob

OpenComputeEngine("Imola", ("-np", "1"))
OpenDatabase("Imola:/Users/johngibbs/Desktop/test.h5", 0)
# ==============================================================
# Solid/liquid interfaces:
print('making solid/liquid interface')
AddPlot("Contour", "phi", 1, 1)
SetActivePlots(0)
ContourAtts = ContourAttributes()

ContourAtts.contourValue = (0.0)
ContourAtts.contourMethod = ContourAtts.Value  # Level, Value, Percent

ContourAtts.colorType = ContourAtts.ColorBySingleColor  # ColorBySingleColor, ColorByMultipleColors, ColorByColorTable
ContourAtts.singleColor = (255, 153, 0, 255)

SetPlotOptions(ContourAtts)
# ==============================================================
# Misc display things:
print('Changing plot settings')
AnnotationAtts = AnnotationAttributes()

AnnotationAtts.axes3D.triadFlag = 1
AnnotationAtts.axes3D.bboxFlag = 1

AnnotationAtts.axes3D.xAxis.title.visible = 0
AnnotationAtts.axes3D.xAxis.label.visible = 1
AnnotationAtts.axes3D.xAxis.grid = 0

AnnotationAtts.axes3D.yAxis.title.visible = 0
AnnotationAtts.axes3D.yAxis.label.visible = 1
AnnotationAtts.axes3D.yAxis.grid = 0

AnnotationAtts.axes3D.zAxis.title.visible = 0
AnnotationAtts.axes3D.zAxis.label.visible = 1
AnnotationAtts.axes3D.zAxis.grid = 0

AnnotationAtts.axes3D.setBBoxLocation = 0
AnnotationAtts.axes3D.bboxLocation = (0, 1, 0, 1, 0, 1)
AnnotationAtts.userInfoFlag = 0
AnnotationAtts.databaseInfoFlag = 1
AnnotationAtts.timeInfoFlag = 1
AnnotationAtts.legendInfoFlag = 0
SetAnnotationAttributes(AnnotationAtts)
# ==============================================================
print('Drawing the plot')
DrawPlots()
# ==============================================================
# Set options to render to png files.
print('Setting up save destination')
swin_atts = SaveWindowAttributes()
swin_atts.outputToCurrentDirectory = False
swin_atts.outputDirectory = "/Users/johngibbs/Desktop/AlCu-60-d_0040/"
swin_atts.fileName = "AlCu-60-d_0040_"
swin_atts.family = 1
swin_atts.format = swin_atts.PNG
swin_atts.width  = 2048
swin_atts.height = 2048
SetSaveWindowAttributes(swin_atts)

if os.path.exists(swin_atts.outputDirectory) == True:
    os.system('rm -rf %s' % swin_atts.outputDirectory)

os.makedirs(swin_atts.outputDirectory)
# ==============================================================
print('Setting initial view')
view3d = GetView3D()
view3d.viewNormal = (+0.0, +0.0, +0.0)
view3d.viewUp     = (-1.0, +0.0, +0.0)
view3d.viewAngle  = +10.0
view3d.imageZoom  = +1.0
SetView3D(view3d)

n = 720
C = 1.0

mid = (n - 1.0) / 2.0

print('Starting rotation')
for i in range(n):
    print('    %3i of %3i' % (i+1, n))
    theta = pi * (i - mid) / mid
    y = C * sin(theta)
    z = C * cos(theta)
    x = -0.25#sqrt(1.0 - y**2 - z**2 + 1e-6)
    # 
    view3d.viewNormal = (x, y, z)
    SetView3D(view3d)
    SaveWindow()
    # 
    #print('%+6.2f %+6.2f %+6.2f' % (x , y, z) )

