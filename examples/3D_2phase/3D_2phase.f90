PROGRAM main
!=== Segmentation code libraries ===============================================
  USE pscs_3D
!=== Variables =================================================================
  IMPLICIT NONE

  INTEGER  ::  nx, ny, nz, i, iter
  INTEGER  ::  nx_full, ny_full, nz_full
  INTEGER  ::  mpi_err, hdf_err

  INTEGER  ::  phi_ds_it, rec_ds_it, dif_ds_it
  REAL(kind=MyR)  ::  phi_ds_dt, rec_ds_dt, dif_ds_dt

  REAL(kind=MyR)  ::  rho_1, rho_2, min_val, max_val
  REAL(kind=MyR), DIMENSION(:,:,:), ALLOCATABLE  ::  rho, rec, dif
  REAL(kind=MyR), DIMENSION(:,:,:), ALLOCATABLE  ::  phi
  LOGICAL, DIMENSION(:,:,:), ALLOCATABLE  ::  mask

  CHARACTER(len=4)  ::  time_str
  CHARACTER(len=char_len)  ::  filename, out_file, fn_fmt = "(A, '/', A, '_', I4.4, '.h5')"
!=== Open HDF interface ========================================================
  CALL h5open_f(hdf_err)
!=== Open MPI interface ========================================================
  CALL MPI_Init ( mpi_err )
  CALL MPI_Comm_rank ( MPI_COMM_WORLD, rank, mpi_err )
  CALL MPI_Comm_size ( MPI_COMM_WORLD, np, mpi_err )
!=== Get dimensions and allocate arrays ========================================
! Read the input parameters from 'input_file.txt'
  CALL ReadInputs ()

  nx = mpi_lim_max(1) - mpi_lim_min(1) + 1
  ny = mpi_lim_max(2) - mpi_lim_min(2) + 1
  nz = mpi_lim_max(3) - mpi_lim_min(3) + 1

  IF (rank == 0) PRINT *, 'allocate arrays'
  ALLOCATE( rho  ( 1-gc:nx+gc, 1-gc:ny+gc, 1-gc:nz+gc ) )
  ALLOCATE( rec  ( 1-gc:nx+gc, 1-gc:ny+gc, 1-gc:nz+gc ) )
  ALLOCATE( dif  ( 1-gc:nx+gc, 1-gc:ny+gc, 1-gc:nz+gc ) )
  ALLOCATE( phi  ( 1-gc:nx+gc, 1-gc:ny+gc, 1-gc:nz+gc ) )
  ALLOCATE( mask ( 1-gc:nx+gc, 1-gc:ny+gc, 1-gc:nz+gc ) )
  CALL MPI_barrier(MPI_COMM_WORLD, mpi_err)
!=== Set timesteps values and number of iterations =============================
  phi_ds_dt = min(0.10, phi_smoothing)
  rec_ds_dt = min(0.10, rec_smoothing)
  dif_ds_dt = min(0.10, dif_smoothing)
  
  phi_ds_it = int(phi_smoothing / (phi_ds_dt + 1e-9))
  rec_ds_it = int(rec_smoothing / (rec_ds_dt + 1e-9))
  dif_ds_it = int(dif_smoothing / (dif_ds_dt + 1e-9))
  
  ! Do some more things with the number of iterations and timesteps to ensure
  ! that the total amount of smoothing time requested actually gets done
  
!=== Read the data =============================================================
  WRITE (filename, fn_fmt) TRIM(input_dir), TRIM(file_basename), time
  
  IF (rank == 0) WRITE (*,*) 'Reading the data from: ', TRIM(filename)
  CALL read_3D_to_3D(filename, rho_dset, rho, mpi_lim_min, mpi_lim_max)
  
  IF (rank == 0) PRINT *, 'Apply BCs'
  CALL const_slope_BC(rho)
!=== Initialize the SDFs from rho ==============================================
  phi = rho - phi1_threshold

  rho_1 = 1.00 * parallel_masked_mean(rho, mask=(phi < 0))
  rho_2 = 1.00 * parallel_masked_mean(rho, mask=(phi > 0))
  
  IF (rank == 0) WRITE (*,"(2X, A, F6.3)") 'rho_1:', rho_1
  IF (rank == 0) WRITE (*,"(2X, A, F6.3)") 'rho_2:', rho_2
  
  ! Scale phi to have correct gradients
  phi = phi * 0.5 / (rho_2 - rho_1)

  IF (rank == 0) WRITE (*,*) 'Reinitializing phi'
  phi = reinit(phi, 40)
!=== Iteratively smooth ========================================================
  DO iter = 1, iterations
    IF (rank == 0) WRITE (*,*)
    IF (rank == 0) WRITE (*,'(2X, I2, A, I2)', advance='no') iter, '/', iterations
    ! Update rec
      WHERE (phi < 0) rec = rho_1
      WHERE (phi > 0) rec = rho_2
      rec = ds(rec, rec_ds_it, rec_ds_dt)
    ! Calculate error term
      dif = ds(rho - rec, dif_ds_it, dif_ds_dt)
    ! Update phi
      phi = phi + update_dt * dif
      
      min_val = parallel_masked_min(dif, mask=(abs(phi) < 2.0))
      max_val = parallel_masked_max(dif, mask=(abs(phi) < 2.0))
      IF (rank == 0) WRITE (*,'(2X, A, F6.2, F6.2)', advance='no') 'rho_err:', min_val, max_val
    ! Reinitialize phi
      phi = reinit(phi, 20)
    ! Smooth phi
      !phi = mmc(phi, phi_ds_it, phi_ds_dt)
      phi = ds(phi, phi_ds_it, phi_ds_dt)
    ! Update rho
      rho_1 = 1.00 * parallel_masked_mean(rho, mask=(phi < 0))
      rho_2 = 1.00 * parallel_masked_mean(rho, mask=(phi > 0))
      
      IF (rank == 0) WRITE (*,'(2X, A, F6.3)', advance='no') 'rho_1=', rho_1
      IF (rank == 0) WRITE (*,'(2X, A, F6.3)', advance='no') 'rho_2=', rho_2
  END DO
  IF (rank == 0) WRITE (*,*)
!=== Write outputs =============================================================
  WRITE (out_file, fn_fmt) TRIM(output_dir), TRIM(file_basename), time
  
  CALL write_3D_to_3D(out_file, rho_dset, rho, dims_full)
  CALL write_3D_to_3D(out_file, phi1_dset, phi, dims_full)
  CALL MPI_barrier(MPI_COMM_WORLD, mpi_err)

!=== Close HDF interface =======================================================
  !PRINT *, 'close hdf'
  CALL h5close_f(hdf_err)
!=== Close MPI interface =======================================================
  !PRINT *, 'close mpi'
  CALL MPI_FINALIZE (mpi_err)

  DEALLOCATE(rho, phi)

END PROGRAM main
