PROGRAM main
!=== Segmentation code libraries ===============================================
  USE pscs_4D
!=== Variables =================================================================
  IMPLICIT NONE
  INTEGER  ::  nx, ny, nz, nt, i, iter, time!, t_min=25
  INTEGER  ::  nx_full, ny_full, nz_full, nt_full
  INTEGER  ::  mpi_err, hdf_err

  INTEGER  ::  phi_ds_it, phi_mmc_it, rec_ds_it, dif_ds_it
  REAL(kind=MyR)  ::  phi_ds_dt, phi_mmc_dt, rec_ds_dt, dif_ds_dt

  REAL(kind=MyR)  ::  rho_1, rho_2, rho_3, min_val, max_val
  REAL(kind=MyR), DIMENSION(:,:,:,:), ALLOCATABLE  ::  rho, rec, dif
  REAL(kind=MyR), DIMENSION(:,:,:,:), ALLOCATABLE  ::  sol, liq, out
  REAL(kind=MyR), DIMENSION(:,:,:,:), ALLOCATABLE  ::  phi_1, phi_2
  LOGICAL, DIMENSION(:,:,:,:), ALLOCATABLE  ::  mask

  CHARACTER(len=4)  ::  time_str
  CHARACTER(len=char_len)  ::  filename, out_file, fn_fmt = "(A, '/', A, '_', I4.4, '.h5')"
!=== Open HDF interface ========================================================
  CALL h5open_f(hdf_err)
!=== Open MPI interface ========================================================
  CALL MPI_Init ( mpi_err )
  CALL MPI_Comm_rank ( MPI_COMM_WORLD, rank, mpi_err )
  CALL MPI_Comm_size ( MPI_COMM_WORLD, np, mpi_err )
!=== Get dimensions and allocate arrays ========================================
! Read the input parameters from 'input_file.txt'
  CALL ReadInputs ()

  nx = mpi_lim_max(1) - mpi_lim_min(1) + 1
  ny = mpi_lim_max(2) - mpi_lim_min(2) + 1
  nz = mpi_lim_max(3) - mpi_lim_min(3) + 1
  nt = mpi_lim_max(4) - mpi_lim_min(4) + 1

  IF (rank == 0) PRINT *, 'allocate arrays'
  ALLOCATE( rho     ( 1-gc:nx+gc, 1-gc:ny+gc, 1-gc:nz+gc, 1-gc:nt+gc ) )
  ALLOCATE( rec     ( 1-gc:nx+gc, 1-gc:ny+gc, 1-gc:nz+gc, 1-gc:nt+gc ) )
  ALLOCATE( dif     ( 1-gc:nx+gc, 1-gc:ny+gc, 1-gc:nz+gc, 1-gc:nt+gc ) )
  ALLOCATE( phi_1   ( 1-gc:nx+gc, 1-gc:ny+gc, 1-gc:nz+gc, 1-gc:nt+gc ) )
  ALLOCATE( phi_2   ( 1-gc:nx+gc, 1-gc:ny+gc, 1-gc:nz+gc, 1-gc:nt+gc ) )
  ALLOCATE( mask    ( 1-gc:nx+gc, 1-gc:ny+gc, 1-gc:nz+gc, 1-gc:nt+gc ) )
  CALL MPI_barrier(MPI_COMM_WORLD, mpi_err)
!=== Set timesteps values and number of iterations =============================
  phi_mmc_dt = 0.1
  phi_ds_dt = 0.1
  rec_ds_dt = 0.1
  dif_ds_dt = 0.1
  
  phi_mmc_it = int(phi_mmc_time / phi_mmc_dt)
  phi_ds_it = int(phi_ds_time / phi_ds_dt)
  rec_ds_it = int(rec_smoothing / rec_ds_dt)
  dif_ds_it = int(dif_smoothing / dif_ds_dt)
!=== Read the data =============================================================
  IF (rank == 0) PRINT *, 'read data'
  DO time = 1, nt
      WRITE (filename, fn_fmt) TRIM(input_dir), TRIM(file_basename), time - 1 + t_min
      IF (rank == 0) WRITE (*,*) 'Reading the data from: ', TRIM(filename)
      CALL read_3D_to_4D(filename, rho_dset, rho, mpi_lim_min, mpi_lim_max, time)
      CALL MPI_barrier(MPI_COMM_WORLD, mpi_err)
  END DO
  IF (rank == 0) PRINT *, 'Apply BCs'
  CALL const_slope_BC(rho)
!=== Initialize the SDFs from rho ==============================================
  phi_1 = rho - phi1_threshold
  phi_2 = rho - phi2_threshold

  rho_1 = 1.00 * parallel_masked_mean (rho, mask=((phi_1 < 0).AND.(phi_2 < 0)))
  rho_2 = 1.00 * parallel_masked_mean (rho, mask=((phi_1 > 0).AND.(phi_2 < 0)))
  rho_3 = 1.00 * parallel_masked_mean (rho, mask=((phi_1 > 0).AND.(phi_2 > 0)))
  
  IF (rank == 0) WRITE (*,"(2X, A, F6.3)") 'rho_1:', rho_1
  IF (rank == 0) WRITE (*,"(2X, A, F6.3)") 'rho_2:', rho_2
  IF (rank == 0) WRITE (*,"(2X, A, F6.3)") 'rho_3:', rho_3
  
  ! Scale phi_1 and phi_2 to have correct gradients
  phi_1 = phi_1 * 0.5 / (rho_3 - rho_1)
  phi_2 = phi_2 * 0.5 / (rho_3 - rho_1)

  IF (rank == 0) WRITE (*,*) 'Reinitializing phi_1'
  phi_1 = reinit(phi_1, 40)
  
  IF (rank == 0) WRITE (*,*) 'Reinitializing phi_2'
  phi_2 = reinit(phi_2, 40)
!=== Remove small features from phi_1 ==========================================
  IF (rank == 0) WRITE (*,*) 'Morph close/open on phi_1'
  phi_1 = ds(phi_1, 10, 0.1, D=(/Dx, Dy, Dz, Dt/))
  phi_1 = reinit(phi_1, 20)

  phi_1 = reinit(phi_1 - 5, 20)
  phi_1 = reinit(phi_1 + 5, 20)
!
!  phi_1 = reinit(phi_1 + 5, 20)
!  phi_1 = reinit(phi_1 - 5, 20)
!=== Calculate constant rho values =============================================
  rho_1 = 1.00 * parallel_masked_mean (rho, mask=((phi_1 < 0).AND.(phi_2 < 0)))
  rho_2 = 1.00 * parallel_masked_mean (rho, mask=((phi_1 > 0).AND.(phi_2 < 0)))
  rho_3 = 1.00 * parallel_masked_mean (rho, mask=((phi_1 > 0).AND.(phi_2 > 0)))

  IF (rank == 0) WRITE (*,'(2X, A, F6.3)') 'rho_1=', rho_1
  IF (rank == 0) WRITE (*,'(2X, A, F6.3)') 'rho_2=', rho_2
  IF (rank == 0) WRITE (*,'(2X, A, F6.3)') 'rho_3=', rho_3
  IF (rank == 0) WRITE (*,*)
!=== Iteratively smooth ========================================================
  DO iter = 1, iterations
    IF (rank == 0) WRITE (*,'(2X, I2, A, I2)', advance='no') iter, '/', iterations
    ! Update rec
      WHERE ((phi_1 < 0) .AND. (phi_2 < 0)) rec = rho_1
      WHERE ((phi_1 > 0) .AND. (phi_2 < 0)) rec = rho_2
      WHERE ((phi_1 > 0) .AND. (phi_2 > 0)) rec = rho_3
      rec = ds(rec, rec_ds_it, rec_ds_dt, D=(/1., 1., 1., 0./))
    ! Calculate error term
      dif = ds(rho - rec, dif_ds_it, dif_ds_dt, D=(/0.50, 0.50, 0.50, 1.00/))
    ! Update phi
      phi_1 = phi_1 + update_dt * dif
      phi_2 = phi_2 + update_dt * dif
      
      min_val = parallel_masked_min(dif, mask=(abs(phi_2) < 2.0))
      max_val = parallel_masked_max(dif, mask=(abs(phi_2) < 2.0))
      IF (rank == 0) WRITE (*,'(2X, A, F6.2, F6.2)', advance='no') 'rho_err:', min_val, max_val
    ! Reinitialize phi
      phi_1 = reinit(phi_1, 10)
      phi_2 = reinit(phi_2, 10)
    ! Smooth phi
      phi_1 = mmc(phi_1, phi_mmc_it, phi_mmc_dt)
      phi_2 = mmc(phi_2, phi_mmc_it, phi_mmc_dt)
      phi_1 = ds(phi_1, phi_ds_it, phi_ds_dt, D=(/Dx, Dy, Dz, Dt/))
      phi_2 = ds(phi_2, phi_ds_it, phi_ds_dt, D=(/Dx, Dy, Dz, Dt/))
    ! Update rho
      rho_1 = 1.00 * parallel_masked_mean (rho, mask=((phi_1 < 0).AND.(phi_2 < 0)))
      rho_2 = 1.00 * parallel_masked_mean (rho, mask=((phi_1 > 0).AND.(phi_2 < 0)))
      rho_3 = 1.00 * parallel_masked_mean (rho, mask=((phi_1 > 0).AND.(phi_2 > 0)))
      
      IF (rank == 0) WRITE (*,'(2X, A, F6.3)', advance='no') 'rho_1=', rho_1
      IF (rank == 0) WRITE (*,'(2X, A, F6.3)', advance='no') 'rho_2=', rho_2
      IF (rank == 0) WRITE (*,'(2X, A, F6.3)', advance='no') 'rho_3=', rho_3
      IF (rank == 0) WRITE (*,*)
  END DO
!=== Write outputs =============================================================
  DO time = 1, nt
    WRITE (out_file, fn_fmt) TRIM(output_dir), TRIM(file_basename), time - 1 + t_min
    !IF (rank == 0) WRITE (*,*) 'Saving data to: ', TRIM(out_file)
    
    CALL write_4D_to_3D(out_file, rho_dset, rho, time, dims_full)
    CALL write_4D_to_3D(out_file, phi1_dset, phi_1, time, dims_full)
    CALL write_4D_to_3D(out_file, phi2_dset, phi_2, time, dims_full)
    CALL MPI_barrier(MPI_COMM_WORLD, mpi_err)
  END DO
!=== Close HDF interface =======================================================
  !PRINT *, 'close hdf'
  CALL h5close_f(hdf_err)
!=== Close MPI interface =======================================================
  !PRINT *, 'close mpi'
  CALL MPI_FINALIZE (mpi_err)

  DEALLOCATE(rho, phi_1, phi_2)
  !DEALLOCATE(out, sol, liq)

END PROGRAM main
